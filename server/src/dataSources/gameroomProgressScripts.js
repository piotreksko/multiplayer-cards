import uuidv4 from "uuid/v4";
import {
  CardRank,
  CardSuit,
  cardRanks,
  cardRanksArray,
  cardSuitsArray,
  rankValues,
} from "../constants/cards";
import { shuffle } from "lodash";

const createDeck = () => {
  const deck = [];
  cardRanksArray.forEach((rank) =>
    cardSuitsArray.forEach((suit) => deck.push({ rank, suit }))
  );
  return deck;
};

export const sortCards = (cards = []) => {
  const sortedBySuit = cards.sort((a, b) => {
    return cardSuitsArray.indexOf(a.suit) - cardSuitsArray.indexOf(b.suit);
  });
  const sortedByRank = sortedBySuit.sort(function (a, b) {
    return cardRanksArray.indexOf(b.rank) - cardRanksArray.indexOf(a.rank);
  });

  return sortedByRank;
};

const dealCards = (players, decksAmount = 1) => {
  let allCards = [];

  for (let i = 0; i < decksAmount; i++) {
    allCards.push(...createDeck());
  }

  allCards = allCards.map((card, index) => ({
    ...card,
    _id: String(index + 1),
  }));
  allCards = shuffle(allCards);

  let currentCardIndex = 0;
  let nextPlayerIndex = 0;
  while (currentCardIndex < allCards.length) {
    players[nextPlayerIndex].cards.push(allCards[currentCardIndex]);
    currentCardIndex++;
    nextPlayerIndex++;
    if (nextPlayerIndex > players.length - 1) {
      nextPlayerIndex = 0;
    }
  }
};

const sortPlayersCards = (players) => {
  players.forEach((player) => {
    player.cards = sortCards(player.cards);
  });
};

const getPlayersOrder = (players, lastGameWinners, grandmaster) => {
  const playersFromLastGame = lastGameWinners.filter(({ _id }) =>
    players.some((player) => player._id)
  );
  const newPlayers = players.filter(
    ({ _id }) => !lastGameWinners.some((player) => player._id === _id)
  );

  const playersForGameStart = [...playersFromLastGame, ...newPlayers];

  const lastPlayerIndex = playersForGameStart[lastGameWinners.length - 1];

  let losers = grandmaster
    ? playersForGameStart.slice(-2)
    : playersForGameStart.slice(-1);
  let winners = grandmaster
    ? playersForGameStart.slice(0, 2)
    : playersForGameStart.slice(0, 1);

  let playersInbetween = playersForGameStart.filter(
    (player) =>
      losers.some((loser) => loser._id !== player._id) &&
      winners.some((winner) => winner._id !== player._id)
  );

  const addSwapValue = (players, value) =>
    players.map((player) => ({ ...player, hasToSwap: value }));

  const addSwapOptions = (players, swapsBestCards) =>
    players
      .reverse()
      .map((player, index) => ({
        ...player,
        swapsBestCards,
        cardsToSwapCount: index + 1,
      }))
      .reverse();

  losers = addSwapOptions(losers, true);
  winners = addSwapOptions(winners, false);

  const newPlayersOrder = [
    ...addSwapValue(losers, true),
    ...addSwapValue(playersInbetween.reverse(), false),
    ...addSwapValue(winners, true),
  ];

  return newPlayersOrder;
};

const createPlayersWithShuffledCards = (
  initialPlayers,
  lastGameWinners = [],
  grandmaster,
  decksAmount
) => {
  const playersInOrder =
    lastGameWinners.length > 0
      ? getPlayersOrder(initialPlayers, lastGameWinners, grandmaster)
      : initialPlayers;
  const players = playersInOrder.map((player) => ({
    ...player,
    cards: [],
  }));

  dealCards(players, decksAmount);
  sortPlayersCards(players);
  return players;
};

export const createGameProgress = ({
  players,
  lastGameWinners = [],
  grandmaster = false,
  decksAmount,
}) => ({
  _id: uuidv4(),
  players: createPlayersWithShuffledCards(
    players,
    lastGameWinners,
    grandmaster,
    decksAmount
  ),
  pile: [],
  lastThreeTurns: [],
  lastPlayerId: "",
  nextPlayerId: lastGameWinners.length
    ? lastGameWinners[lastGameWinners.length - 1]._id
    : "",
  stairsActivated: false,
  winners: [],
  lastGameWinners,
  stairsActivated: false,
  stairsStep: 0,
  grandmaster,
  cardsSwapDone: !!lastGameWinners.length ? false : true,
  isNewRound: true,
  isGameOver: false,
});

const getUniqueRanks = (cards) => [
  // eslint-disable-next-line no-undef
  ...new Set(cards.map((card) => card.rank)),
];

export const checkStairs = (lastThreeTurns, isNewRound) => {
  const [firstTurn, secondTurn, thirdTurn] = lastThreeTurns;

  if (lastThreeTurns.length === 3) {
    const test = getUniqueRanks(secondTurn.cards);
    const lastTwoTurnsHadSameAmount =
      getUniqueRanks(secondTurn.cards).length === 1 &&
      getUniqueRanks(thirdTurn.cards).length === 1 &&
      secondTurn.cards.length === thirdTurn.cards.length;
    if (lastTwoTurnsHadSameAmount) {
      const [
        { cards: firstTurnCards },
        { cards: secondTurnCards },
        { cards: thirdTurnCards },
      ] = lastThreeTurns;

      const firstMoveWeight = cardRanksArray.indexOf(
        firstTurnCards[firstTurnCards.length - 1].rank
      );
      const secondMoveWeight = cardRanksArray.indexOf(secondTurnCards[0].rank);
      const thirdMoveWeight = cardRanksArray.indexOf(thirdTurnCards[0].rank);

      const allMovesHadSameCardsAmount =
        firstTurnCards.length === secondTurnCards.length &&
        firstTurnCards.length === thirdTurnCards.length;
      const isStepsDifferenceEqual =
        secondMoveWeight - firstMoveWeight ===
        thirdMoveWeight - secondMoveWeight;

      const wereLastTwoTurnsNewRound =
        secondTurn.isNewRound || thirdTurn.isNewRound;

      if (
        isStepsDifferenceEqual &&
        allMovesHadSameCardsAmount &&
        !wereLastTwoTurnsNewRound
      ) {
        const stairsActivated = true;
        const stairsStep = thirdMoveWeight - secondMoveWeight;
        return {
          stairsActivated,
          stairsStep,
        };
      }
    }
  }

  const lastTurn = lastThreeTurns[lastThreeTurns.length - 1];
  const preLastTurn = lastThreeTurns[lastThreeTurns.length - 2] || {
    cards: [],
  };

  const uniqueRanksLastTurn = getUniqueRanks(lastTurn.cards);
  const uniqueRanksLastTwoTurns = preLastTurn
    ? getUniqueRanks([...preLastTurn.cards, ...lastTurn.cards])
    : [];
  const uniqueRanksLastThreeTurns = [
    ...getUniqueRanks(firstTurn.cards),
    ...uniqueRanksLastTwoTurns,
  ];

  const stepsFromLastTwo = uniqueRanksLastTwoTurns.map(
    (rank) => rankValues[rank]
  );
  const stepsFromLastThree = uniqueRanksLastThreeTurns.map(
    (rank) => rankValues[rank]
  );
  const stepDifferencesFromLastTwo = stepsFromLastTwo.map((step, index) => {
    const nextStep = stepsFromLastTwo[index + 1];
    return nextStep ? step - nextStep : 0;
  });
  const stepDifferencesFromLastThree = stepsFromLastThree.map((step, index) => {
    const nextStep = stepsFromLastThree[index + 1];
    return nextStep ? step - nextStep : 0;
  });
  stepDifferencesFromLastTwo.pop();
  stepDifferencesFromLastThree.pop();
  const uniqueStepsFromLastTwo = [...new Set(stepDifferencesFromLastTwo)];
  const uniqueStepsFromLastThree = [...new Set(stepDifferencesFromLastThree)];

  if (
    uniqueRanksLastTurn.length === 3 ||
    (uniqueRanksLastTwoTurns.length === 4 &&
      !lastTurn.isNewRound &&
      uniqueStepsFromLastTwo.length === 1) ||
    (uniqueRanksLastThreeTurns.length === 5 &&
      !lastTurn.isNewRound &&
      uniqueStepsFromLastThree.length === 1)
  ) {
    const stairsActivated = true;
    const stairsStep =
      rankValues[uniqueRanksLastTwoTurns[uniqueRanksLastTwoTurns.length - 1]] -
      rankValues[uniqueRanksLastTwoTurns[uniqueRanksLastTwoTurns.length - 2]];
    return { stairsActivated, stairsStep };
  }

  return { stairsActivated: false, stairsStep: 0 };
};

export const checkAllowedCardsAmount = (lastThreeTurns) => {
  const lastTurn = lastThreeTurns[lastThreeTurns.length - 1];
  if (!lastTurn.cards.length) {
    return 1;
  }
  // eslint-disable-next-line no-undef
  const uniqueRanks = [...new Set(lastTurn.cards.map((card) => card.rank))];
  const cardsByRankAmount = lastTurn.cards.filter(
    (card) => card.rank === uniqueRanks[0]
  ).length;
  return cardsByRankAmount;
};

export const getNextPlayerId = (
  players,
  winners,
  userId,
  lastPlayerId,
  skippedPlayerId
) => {
  const currentPlayerIndex = players.findIndex(({ _id }) => _id === userId);
  const nextPlayerIndex =
    currentPlayerIndex + 1 === players.length ? 0 : currentPlayerIndex + 1;
  const nextPlayerId = players[nextPlayerIndex]._id;
  const wonPlayer = winners.find(({ _id }) => _id === nextPlayerId);
  const hasNextPlayerWon = !!wonPlayer;
  const toSkip = wonPlayer && wonPlayer.skipAfterWinning && wonPlayer._id;
  if (hasNextPlayerWon) {
    return getNextPlayerId(
      players,
      winners,
      nextPlayerId,
      lastPlayerId,
      toSkip
    );
  }
  return {
    nextPlayerId,
    lastPlayerId: skippedPlayerId ? nextPlayerId : lastPlayerId,
    skippedPlayerId,
  };
};

export const updateLastThreeTurns = (lastThreeTurns, usedCards, isNewRound) => {
  if (lastThreeTurns.length === 3) {
    lastThreeTurns.shift();
  }
  lastThreeTurns.push({ cards: usedCards, isNewRound });
  return lastThreeTurns;
};

export const markAvailableCards = (progress, userid) => {
  const player = progress.players.find(({ _id }) => _id === userid);
  const { cards } = player;

  const isPlayerTurn = progress.nextPlayerId === userid;
  const isVeryFirstRound =
    !progress.nextPlayerId && !progress.lastGameWinners.length;

  if (
    (!isPlayerTurn && !isVeryFirstRound && !player.hasToSwap) ||
    progress.isGameOver
  ) {
    return cards;
  }

  const wereTheyUsedLastTurn = (requiredRank, requiredAmount) => {
    const { lastThreeTurns } = progress;
    const lastCardsUsed = lastThreeTurns[lastThreeTurns.length - 1];
    if (!lastCardsUsed) {
      return false;
    }
    const cardsWithRankUsed = lastCardsUsed.filter(
      ({ rank }) => rank === requiredRank
    ).length;

    return cardsWithRankUsed === requiredAmount;
  };

  const wereThreeSixPlayed = wereTheyUsedLastTurn(cardRanks[6], 3);
  const wereFourJacksPlayed = wereTheyUsedLastTurn(cardRanks.JACK, 4);
  const wereFiveAcesPlayed = wereTheyUsedLastTurn(cardRanks.ACE, 5);

  const hasSufficientCardsAmount = (rank, amount) => {
    const possibleCards = cards.filter((card) => card.rank === rank);
    const hasSufficientAmount = possibleCards.length >= amount;
    return hasSufficientAmount;
  };

  const markCardsByRank = (cardsToMark, rank) =>
    cardsToMark.map((card) => ({
      ...card,
      available: card.available || card.rank === rank,
    }));

  const markCombo = (cardsToCheck, requiredRank, requiredAmount) => {
    const requiredCardsLength = cardsToCheck.filter(
      ({ rank }) => rank === requiredRank
    ).length;
    if (requiredCardsLength === requiredAmount) {
      return markCardsByRank(cardsToCheck, requiredRank);
    }
    return cardsToCheck;
  };

  const markAllCombos = (cardsToCheck) => {
    let comboCardsMarked = cardsToCheck;
    if (!wereThreeSixPlayed && !wereFourJacksPlayed && !wereFiveAcesPlayed) {
      comboCardsMarked = markCombo(comboCardsMarked, cardRanks[6], 3);
    }
    if (!wereFourJacksPlayed && !wereFiveAcesPlayed) {
      comboCardsMarked = markCombo(comboCardsMarked, cardRanks.JACK, 4);
    }
    if (!wereFiveAcesPlayed) {
      comboCardsMarked = markCombo(comboCardsMarked, cardRanks.ACE, 5);
    }
    return comboCardsMarked;
  };

  const markThrees = (cardsToCheck) => {
    const playerHasThreeOfClubs = cardsToCheck.some(
      ({ rank, suit }) => rank === cardRanks[3] && suit === CardSuit.CLUBS
    );
    return playerHasThreeOfClubs
      ? cardsToCheck.map((card) => ({
          ...card,
          available: card.rank === cardRanks[3],
        }))
      : cards;
  };

  const markTwos = (cardsToCheck) => {
    const { lastThreeTurns } = progress;
    const lastTurnCards = lastThreeTurns[lastThreeTurns.length - 1];

    if (!lastTurnCards || !lastTurnCards.length) return cardsToCheck;

    const wereTwosUsedLastTurn = lastTurnCards[0].rank === CardRank.TWO;
    const redTwosUsedLastTurn = lastTurnCards.filter(
      ({ rank, suit }) => suit === CardSuit.HEARTS && rank === CardRank.TWO
    );
    const normalTwosUsedLastTurn = lastTurnCards.filter(
      ({ rank, suit }) => suit !== CardSuit.HEARTS && rank === CardRank.TWO
    );

    const allTwos = cards.filter(({ rank }) => rank === CardRank.TWO);
    const redTwosAmount = allTwos.filter(
      ({ suit }) => suit === CardSuit.HEARTS
    ).length;

    if (wereTwosUsedLastTurn) {
      if (!redTwosAmount) {
        return cardsToCheck;
      }
      if (
        lastTurnCards.length <= allTwos.length &&
        normalTwosUsedLastTurn &&
        redTwosUsedLastTurn.length < redTwosAmount
      ) {
        // Only when opponent used more than 1 normal two, you can beat him using a normal two + reds
        const canUseNormalTwo = normalTwosUsedLastTurn.length > 1;
        const isCardAvailable = (card) => {
          if (card.available) {
            return true;
          }
          if (card.rank !== CardRank.TWO) {
            return false;
          }
          if (!canUseNormalTwo && card.suit !== CardSuit.HEARTS) {
            return false;
          }
          // only 2 of hearts should reach this return
          return true;
        };
        return cardsToCheck.map((card) => ({
          ...card,
          available: isCardAvailable(card),
        }));
      }
      return cardsToCheck;
    }
    const normalTwosAmount = allTwos.filter(
      ({ suit }) => suit !== CardSuit.HEARTS
    ).length;
    const hasSufficientCombinedPower =
      redTwosAmount.length * 2 + allTwos.length - redTwosAmount >
      lastTurnCards.length;
    const canUseNormalTwo =
      hasSufficientCombinedPower || normalTwosAmount >= lastTurnCards.length;
    const canUseRedTwo =
      hasSufficientCombinedPower || redTwosAmount * 2 >= lastTurnCards.length;

    const isCardAvailable = (card) => {
      if (card.available) {
        return true;
      }
      if (card.rank === CardRank.TWO) {
        if (card.suit === CardSuit.HEARTS) {
          return canUseRedTwo;
        }
        return canUseNormalTwo;
      }
      return false;
    };

    return cardsToCheck.map((card) => ({
      ...card,
      available: isCardAvailable(card),
    }));
  };

  const markCardsToSwap = (cardsToCheck) => {
    if (player.swapsBestCards) {
      const cardsToSwap = cardsToCheck.slice(0, player.cardsToSwapCount);

      return cardsToCheck.map((card) => ({
        ...card,
        available:
          card.available || cardsToSwap.some(({ _id }) => _id === card._id),
      }));
    }
    return cardsToCheck.map((card) => ({
      ...card,
      available: true,
    }));
  };

  const markNormalCards = (cardsToCheck) => {
    const isNewGameBeginning =
      !progress.lastThreeTurns.length || progress.lastPlayerId === userid;

    if (isNewGameBeginning) {
      return cardsToCheck.map((card) => ({ ...card, available: true }));
    }

    const { lastThreeTurns } = progress;
    const lastTurnCardsAmount =
      lastThreeTurns[lastThreeTurns.length - 1].length;

    if (progress.stairsActivated) {
      const lastTurnRank = lastThreeTurns[lastThreeTurns.length - 1][0].rank;
      const lastRankIndex = cardRanksArray.indexOf(lastTurnRank);
      const preLastTurnRank = lastThreeTurns[lastThreeTurns.length - 2][0].rank;
      const preLastIndex = cardRanksArray.indexOf(preLastTurnRank);

      const stepDifference = lastRankIndex - preLastIndex;
      const nextStepRankIndex =
        stepDifference > 0 ? lastRankIndex + stepDifference : 0;
      const nextPossibleRank = cardRanksArray[nextStepRankIndex];

      const hasSufficientCards = hasSufficientCardsAmount(
        nextPossibleRank,
        lastTurnCardsAmount
      );

      return cardsToCheck.map((card) => ({
        ...card,
        available: card.rank === nextPossibleRank && hasSufficientCards,
      }));
    }

    const isCardHigher = (cardRank, rankToCompare) =>
      cardRanksArray.indexOf(cardRank) - cardRanksArray.indexOf(rankToCompare) >
      0;
    const lastTurnRank = lastThreeTurns[lastThreeTurns.length - 1][0].rank;

    return cardsToCheck.map((card) => ({
      ...card,
      available:
        card.available ||
        (isCardHigher(card.rank, lastTurnRank) &&
          hasSufficientCardsAmount(card.rank, lastTurnCardsAmount)),
    }));
  };

  const markCards = () => {
    let cardsToReturn = cards;

    if (isVeryFirstRound) {
      return markThrees(cardsToReturn);
    }

    if (player.hasToSwap) {
      return markCardsToSwap(cardsToReturn);
    }

    cardsToReturn = markAllCombos(cardsToReturn);

    const wasComboUsed =
      wereThreeSixPlayed || wereFourJacksPlayed || wereFiveAcesPlayed;
    const userStartsNewRound =
      progress.lastPlayerId === userid && progress.isNewRound;

    if (!wasComboUsed || userStartsNewRound) {
      // cardsToReturn = markNormalCards(cardsToReturn);
      cardsToReturn = markTwos(cardsToReturn);
    }

    return cardsToReturn;
  };

  return markCards();
};
