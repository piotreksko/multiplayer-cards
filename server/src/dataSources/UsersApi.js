import _ from 'lodash';
import { errorMessages } from '@/constants/errorMessages';
import createUser from '@/resolvers/common/createUser';
import {
  NEW_USER,
  UPDATE_ACTIVE_USERS,
} from '@/resolvers/common/subscriptionNames';
import pubsub from '@/pubsub';

const jwt = require('jsonwebtoken');

class UsersApi {
  constructor() {
    this.activeUsers = [];
    this.users = [];
  }

  getUser(userId) {
    if (!userId) {
      return null;
    }

    const activeUser = this.activeUsers.find((user) => user._id === userId);
    return activeUser;
  }

  getGameroomPlayers(gameroomId) {
    const players = this.players.filter(
      (user) => user.gameroomId === gameroomId,
    );
    return players;
  }

  getActiveUsers() {
    return this.activeUsers;
  }

  logIn(username) {
    const userExists = this.activeUsers.some(
      (user) => user.username === username,
    );

    if (userExists) {
      throw new Error(errorMessages.USERNAME_TAKEN.name);
    }

    const createdUser = createUser(username);
    this.activeUsers = [...this.activeUsers, createdUser];

    pubsub.publish(NEW_USER, {
      newUser: {
        _id: createdUser._id,
        username: createdUser.username,
      },
    });

    pubsub.publish(UPDATE_ACTIVE_USERS, {
      updateActiveUsers: [...this.activeUsers],
    });

    // const token = jwt.sign({ userId: createdUser._id }, process.env.JWT_SECRET);

    // doesnt compile
    // return {...createdUser, token};
    return { ...createdUser };
  }

  logOut(user) {
    //TODO
  }
}

module.exports = { UsersApi };
