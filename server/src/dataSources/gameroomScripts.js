import uuidv4 from 'uuid/v4';
import _ from 'lodash';

export function createMessage(text, createdBy) {
  return {
    _id: uuidv4(),
    text: text || '',
    createdBy: createdBy || '',
    createdAt: new Date(),
  };
}

export const createGameroom = ({ name, host, users, id }) => ({
  _id: id || uuidv4(),
  name,
  host: host || {},
  players: users ? users.map((user) => ({ ...user, isReady: false })) : [host],
  gameProgressId: null,
});

export const removeUserFromHost = (gameroom, userId) => {
  if (gameroom.host._id === userId) {
    gameroom.host = gameroom.players.length ? gameroom.players[0] : {};
  }
};

export const removeUserFromGameroomUsers = (gameroom, userId) =>
  (gameroom.users = gameroom.players.filter((user) => user._id !== userId));

export const removeUserFromGameroom = (gameroom, userId) => {
  removeUserFromHost(gameroom, userId);
  removeUserFromGameroomUsers(gameroom, userId);
};
