import {
  UPDATE_GAMEROOM,
  UPDATE_GAME_PROGRESS,
} from '../resolvers/common/subscriptionNames';
import pubsub from '../pubsub';
import {
  checkStairs,
  checkAllowedCardsAmount,
  createGameProgress,
  getNextPlayerId,
  sortCards,
  updateLastThreeTurns,
} from './gameroomProgressScripts';

class GameProgressApi {
  constructor() {
    this.gameProgresses = [];
  }

  getGameProgress(gameProgressId) {
    const gameProgress = this.gameProgresses.find(
      (progress) => progress._id === gameProgressId,
    );
    return gameProgress;
  }

  updateGameProgress(gameroom) {
    pubsub.publish(UPDATE_GAMEROOM, {
      updateGameroom: gameroom,
    });
  }

  startGame(gameroom, lastGameWinners = [], user) {
    // const readyPlayersCount = gameroom.players.filter(({isReady}) => isReady).length;

    // if (readyPlayersCount < 2) {
    //   return false;
    // }
    const canGrandMasterBeApplied =
      !!gameroom.grandmaster && gameroom.players.length > 3;

    const initialSettings = {
      players: gameroom.players,
      lastGameWinners,
      grandmaster: canGrandMasterBeApplied,
      decksAmount: gameroom.decksAmount,
    };

    const newGameProgress = createGameProgress(initialSettings);
    this.gameProgresses.push(newGameProgress);
    return newGameProgress;
  }

  passMove(progressId, user) {
    const progress = this.getGameProgress(progressId);

    const { nextPlayerId, lastPlayerId, skippedPlayerId } = getNextPlayerId(
      progress.players,
      progress.winners,
      user._id,
      progress.lastPlayerId,
    );
    if (skippedPlayerId) {
      progress.winners = progress.winners.map((winner) =>
        winner._id === skippedPlayerId
          ? { ...winner, skipAfterWinning: false }
          : winner,
      );
    }
    progress.nextPlayerId = nextPlayerId;
    progress.lastPlayerId = lastPlayerId;
    progress.isNewRound = progress.lastPlayerId === progress.nextPlayerId;
    if (progress.isNewRound) {
      progress.stairsActivated = false;
    }

    pubsub.publish(UPDATE_GAME_PROGRESS, {
      updateGameProgress: progress,
    });

    return true;
  }

  swapCards(player, usedCards, progress) {
    const { cardsToSwapCount, swapsBestCards } = player;

    if (usedCards.length !== cardsToSwapCount) {
      return new Error('Wrong cards swap amount');
    }

    player.cards = player.cards.filter(
      ({ _id }) => !usedCards.some((card) => card._id === _id),
    );

    const playersSwapping = progress.grandmaster ? 2 : 1;
    const playerToReceiveIndex = swapsBestCards
      ? progress.players.length - 1 + playersSwapping - cardsToSwapCount
      : playersSwapping - cardsToSwapCount;

    const playerToReceive = progress.players[playerToReceiveIndex];
    playerToReceive.cards = sortCards([...playerToReceive.cards, ...usedCards]);
    player.cards = player.cards.filter(
      ({ _id }) => !usedCards.some((card) => card._id === _id),
    );

    player.hasToSwap = false;

    if (!progress.players.some(({ hasToSwap }) => hasToSwap)) {
      progress.cardsSwapDone = true;
    }
    pubsub.publish(UPDATE_GAME_PROGRESS, {
      updateGameProgress: progress,
    });

    return true;
  }

  confirmMove(gameProgressId, cardIds, user) {
    if (!cardIds.length) {
      return new Error(`${user.username} - no cards selected`);
    }
    const progress = this.getGameProgress(gameProgressId);

    const player = progress.players.find(({ _id }) => _id === user._id);
    const usedCards = player.cards.filter(({ _id }) =>
      cardIds.some((id) => id === _id),
    );
    const isFirstMove = !progress.pile.length;

    if (progress.nextPlayerId !== user._id && !isFirstMove) {
      return new Error(`${user.username} - not your turn`);
    }
    if (!usedCards.length) {
      return new Error(`${user.username} - no cards selected`);
    }

    if (!progress.cardsSwapDone && player.hasToSwap) {
      return this.swapCards(player, usedCards, progress);
    }

    player.cards = player.cards.filter(
      ({ _id }) => !cardIds.some((id) => id === _id),
    );

    progress.pile.push(...usedCards.reverse());

    if (!player.cards.length) {
      progress.winners.push({ ...user, skipAfterWinning: true });
    }

    const playersWithCardsLeft = progress.players.filter(
      ({ cards }) => !!cards.length,
    );
    const onePlayerLeft = playersWithCardsLeft.length === 1;

    if (onePlayerLeft) {
      progress.winners.push({
        _id: playersWithCardsLeft[0]._id,
        username: playersWithCardsLeft[0].username,
      });
      progress.isGameOver = true;
      pubsub.publish(UPDATE_GAME_PROGRESS, {
        updateGameProgress: progress,
      });
      return true;
    }

    progress.lastThreeTurns = updateLastThreeTurns(
      progress.lastThreeTurns,
      usedCards,
      progress.isNewRound,
    );
    progress.isNewRound = false;
    const { stairsActivated, stairsStep } = checkStairs(
      progress.lastThreeTurns,
    );
    progress.stairsActivated = stairsActivated;
    progress.stairsStep = stairsStep;
    progress.allowedCardsAmount = checkAllowedCardsAmount(
      progress.lastThreeTurns,
    );
    progress.lastPlayerId = user._id;
    const { nextPlayerId, skippedPlayerId } = getNextPlayerId(
      progress.players,
      progress.winners,
      user._id,
    );
    if (skippedPlayerId) {
      progress.winners = progress.winners.map((winner) =>
        winner._id === skippedPlayerId
          ? { ...winner, skipAfterWinning: false }
          : winner,
      );
    }
    progress.nextPlayerId = nextPlayerId;

    pubsub.publish(UPDATE_GAME_PROGRESS, {
      updateGameProgress: progress,
    });

    return true;
  }
}

module.exports = { GameProgressApi };
