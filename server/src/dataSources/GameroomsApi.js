import _ from 'lodash';
import pubsub from '../pubsub';

import {
  UPDATE_GAMEROOM,
  NEW_MESSAGE,
} from '../resolvers/common/subscriptionNames';
import initialData from '../constants/initialData';
import {
  createGameroom,
  createMessage,
  removeUserFromGameroom,
} from './gameroomScripts';

const generateInitialGamerooms = initialData.gamerooms.map((gameroom) =>
  createGameroom(gameroom),
);

class GameroomsApi {
  constructor() {
    this.gamerooms = generateInitialGamerooms;
  }

  getGamerooms() {
    return this.gamerooms;
  }

  getGameroom(gameroomId) {
    const gameroom = this.gamerooms.find(
      (gameroom) => gameroom._id === gameroomId,
    );
    return gameroom;
  }

  createGameroom(name, host) {
    const gameroom = createGameroom({ name, host });
    this.gamerooms.push(gameroom);
    return gameroom;
  }

  togglePlayerReady(gameroomId, user) {
    const gameroom = this.getGameroom(gameroomId);
    const userToUpdate = gameroom.players.find(({ _id }) => _id === user._id);

    if (userToUpdate.isReady) {
      userToUpdate.isReady = false;
    } else {
      userToUpdate.isReady = true;
    }
    this.updateGameroom(gameroom);
  }

  joinGameroom(gameroomId, user) {
    const gameroom = this.getGameroom(gameroomId);
    gameroom.players.push({ _id: user._id, username: user.username });
    // const newPlayerMessage = createMessage(`${user.username} has joined`, '');
    // gameroom.messages.push(newPlayerMessage);
    if (!gameroom.host._id) {
      gameroom.host = user;
    }
    this.updateGameroom(gameroom);
  }

  leaveGameroom(gameroomId, { _id: userId }) {
    const gameroom = this.getGameroom(gameroomId);
    removeUserFromGameroom(gameroom, userId);
    this.updateGameroom(gameroom);
  }

  sendMessage(gameroomId, text, user) {
    const gameroom = this.getGameroom(gameroomId);
    const createdMessage = createMessage(text, user.username);
    // gameroom.messages = [...gameroom.messages, createdMessage];
    this.updateGameroom(gameroom);
    return createdMessage;
  }

  assignGameProgress(gameroom, progressId) {
    gameroom.gameProgressId = progressId;
    gameroom.gameInProgress = true;
    this.updateGameroom(gameroom);
  }

  updateGameroom(gameroom) {
    pubsub.publish(UPDATE_GAMEROOM, {
      updateGameroom: gameroom,
    });
  }

  // newMessage(gameroomId, message) {
  //   pubsub.publish(NEW_MESSAGE, {
  //     gameroomId,
  //     message,
  //   });
  // }
}

module.exports = { GameroomsApi };
