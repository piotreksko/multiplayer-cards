import pubsub from '@/pubsub';
import {
  NEW_USER,
  UPDATE_ACTIVE_USERS,
} from '@/resolvers/common/subscriptionNames';

const users = {
  Query: {
    getUser: (root, args, { dataSources: { users } }, { user }) => {
      return users.getCurrentPoint(user);
    },
    getActiveUsers: (root, args, { dataSources: { users } }) => {
      return users.getActiveUsers();
    },
  },
  Mutation: {
    userLogIn(root, { username }, { dataSources: { users } }) {
      return users.logIn(username);
    },

    userLogOut(root, args, { dataSources: { users } }, { user }) {
      return users.logOut(user);
    },
  },
  Subscription: {
    updateActiveUsers: {
      subscribe: () => pubsub.asyncIterator([UPDATE_ACTIVE_USERS]),
    },
    newUser: {
      subscribe: () => pubsub.asyncIterator([NEW_USER]),
    },
  },
};

export default users;
