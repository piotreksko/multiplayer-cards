import { withFilter } from 'graphql-subscriptions';
import pubsub from '../pubsub';
import { UPDATE_GAME_PROGRESS } from '../resolvers/common/subscriptionNames';
import { markAvailableCards } from '../dataSources/gameroomProgressScripts';

const getPlayers = (progress, user) => {
  try {
    const { isGameOver, players } = progress;
    const getCardsToShow = (player) => {
      if (isGameOver || player._id === user._id) {
        return player.cards;
      }
      // if (player._id === user._id) {
      //   return markAvailableCards(progress, user._id);
      // }
      return [];
    };

    const filteredPlayers = players.map((player) => ({
      ...player,
      cardsCount:
        player.cardsCount || (player.cards && player.cards.length) || 0,
      cards: getCardsToShow(player),
    }));
    return filteredPlayers;
  } catch (err) {}
};

const gameProgress = {
  GameProgress: {
    players: async (progress, args, { user }, info) =>
      getPlayers(progress, user),
  },
  Query: {
    getGameProgress: (
      root,
      { gameProgressId },
      { dataSources: { gameProgress } },
    ) => {
      return gameProgress.getGameProgress(gameProgressId);
    },
    getProgresses: (root, {}, { dataSources: { gameProgress } }) => {
      return gameProgress.gameProgresses;
    },
  },
  Mutation: {
    confirmMove(
      _,
      { gameProgressId, cardIds },
      { dataSources: { gameProgress }, user },
    ) {
      return gameProgress.confirmMove(gameProgressId, cardIds, user);
    },
    passMove(_, { gameProgressId }, { dataSources: { gameProgress }, user }) {
      return gameProgress.passMove(gameProgressId, user);
    },
  },
  Subscription: {
    updateGameProgress: {
      subscribe: withFilter(
        () => pubsub.asyncIterator([UPDATE_GAME_PROGRESS]),
        (payload, args) => {
          return payload.updateGameProgress._id === args.gameProgressId;
        },
      ),
      resolve: (payload, args, context) => {
        return {
          ...payload.updateGameProgress,
          players: getPlayers(payload.updateGameProgress, context.user),
        };
      },
    },
  },
};

export default gameProgress;
