import { gamerooms } from './gamerooms';

describe('Gamerooms resolver', () => {
  describe('Query.getGamerooms', () => {
    it('should return the list of gamerooms', async () => {
      // mock the data sources object
      const dataSources = {
        gamerooms: {
          getGamerooms: jest
            .fn()
            .mockResolvedValue([{ _id: '123' }, { _id: '456' }]),
        },
      };

      const result = await gamerooms.Query.getGamerooms(
        null,
        {},
        { dataSources },
      );
      expect(result).toEqual([{ _id: '123' }, { _id: '456' }]);
      expect(dataSources.gamerooms.getGamerooms).toHaveBeenCalled();
    });
  });

  describe('Query.getGameroom', () => {
    it('should return the gameroom with the specified id', async () => {
      // mock the data sources object
      const dataSources = {
        gamerooms: {
          getGameroom: jest.fn().mockResolvedValue({ _id: '123' }),
        },
      };

      const result = await gamerooms.Query.getGameroom(
        null,
        { gameroomId: '123' },
        { dataSources },
      );
      expect(result).toEqual({ _id: '123' });
      expect(dataSources.gamerooms.getGameroom).toHaveBeenCalledWith('123');
    });
  });

  describe('Mutation.createGameroom', () => {
    it('should call the createGameroom method on the gamerooms data source and return the created gameroom', async () => {
      // mock the data sources object
      const dataSources = {
        gamerooms: {
          createGameroom: jest.fn().mockResolvedValue({ _id: '123' }),
        },
      };

      const result = await gamerooms.Mutation.createGameroom(
        null,
        { name: 'Test gameroom' },
        { dataSources, user: { _id: 'abc' } },
      );
      expect(result).toEqual({ _id: '123' });
      expect(dataSources.gamerooms.createGameroom).toHaveBeenCalledWith(
        'Test gameroom',
        { _id: 'abc' },
      );
    });
  });

  describe('Mutation.joinGameroom', () => {
    it('should call the joinGameroom method on the gamerooms data source', async () => {
      // mock the data sources object
      const dataSources = {
        gamerooms: {
          joinGameroom: jest.fn(),
        },
      };

      await gamerooms.Mutation.joinGameroom(
        null,
        { gameroomId: '123' },
        { dataSources, user: { _id: 'abc' } },
      );
      expect(dataSources.gamerooms.joinGameroom).toHaveBeenCalledWith('123', {
        _id: 'abc',
      });
    });
  });
});
