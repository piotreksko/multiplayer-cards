import { gameProgress } from './gameProgress';

describe('GameProgress resolver', () => {
  describe('Query.getGameProgress', () => {
    it('should return the game progress with the specified id', async () => {
      // mock the data sources object
      const dataSources = {
        gameProgress: {
          getGameProgress: jest.fn().mockResolvedValue({ _id: '123' }),
        },
      };

      const result = await gameProgress.Query.getGameProgress(
        null,
        { gameProgressId: '123' },
        { dataSources },
      );
      expect(result).toEqual({ _id: '123' });
      expect(dataSources.gameProgress.getGameProgress).toHaveBeenCalledWith(
        '123',
      );
    });
  });

  describe('Query.getProgresses', () => {
    it('should return the list of game progresses', async () => {
      // mock the data sources object
      const dataSources = {
        gameProgress: {
          gameProgresses: [{ _id: '123' }, { _id: '456' }],
        },
      };

      const result = await gameProgress.Query.getProgresses(
        null,
        {},
        { dataSources },
      );
      expect(result).toEqual([{ _id: '123' }, { _id: '456' }]);
    });
  });

  describe('Mutation.confirmMove', () => {
    it('should call the confirmMove method on the gameProgress data source and return the updated game progress', async () => {
      // mock the data sources object
      const dataSources = {
        gameProgress: {
          confirmMove: jest.fn().mockResolvedValue({ _id: '123' }),
        },
      };

      const result = await gameProgress.Mutation.confirmMove(
        null,
        { gameProgressId: '123', cardIds: ['1', '2'] },
        { dataSources, user: { _id: 'abc' } },
      );
      expect(result).toEqual({ _id: '123' });
      expect(dataSources.gameProgress.confirmMove).toHaveBeenCalledWith(
        '123',
        ['1', '2'],
        { _id: 'abc' },
      );
    });
  });

  describe('Mutation.passMove', () => {
    it('should call the passMove method on the gameProgress data source and return the updated game progress', async () => {
      // mock the data sources object
      const dataSources = {
        gameProgress: {
          passMove: jest.fn().mockResolvedValue({ _id: '123' }),
        },
      };

      const result = await gameProgress.Mutation.passMove(
        null,
        { gameProgressId: '123' },
        { dataSources, user: { _id: 'abc' } },
      );
      expect(result).toEqual({ _id: '123' });
      expect(dataSources.gameProgress.passMove).toHaveBeenCalledWith('123', {
        _id: 'abc',
      });
    });
  });

  describe('Subscription.updateGameProgress', () => {
    it('should filter out updates that do not match the specified game progress id', () => {
      const callback = jest.fn();
      const payload = { updateGameProgress: { _id: '123' } };
      const args = { gameProgressId: '456' };

      gameProgress.Subscription.updateGameProgress.subscribe(callback)(
        payload,
        args,
      );
      expect(callback).not.toHaveBeenCalled();
    });

    it('should pass through updates that match the specified game progress id', () => {
      const callback = jest.fn();
      const payload = { updateGameProgress: { _id: '123' } };
      const args = { gameProgressId: '123' };

      gameProgress.Subscription.updateGameProgress.subscribe(callback)(
        payload,
        args,
      );
      expect(callback).toHaveBeenCalledWith(payload);
    });

    it('should return the updated game progress with filtered players', () => {
      const payload = {
        updateGameProgress: { _id: '123', players: [{ _id: 'abc' }] },
      };
      const args = { gameProgressId: '123' };
      const context = { user: { _id: 'abc' } };

      const result = gameProgress.Subscription.updateGameProgress.resolve(
        payload,
        args,
        context,
      );
      expect(result).toEqual({ _id: '123', players: [] });
    });
  });
});
