import { users } from './users';

describe('Users resolver', () => {
  describe('Query.getUser', () => {
    it('should return the current point of the authenticated user', async () => {
      // mock the data sources object
      const dataSources = {
        users: {
          getCurrentPoint: jest.fn().mockResolvedValue(100),
        },
      };

      const result = await users.Query.getUser(
        null,
        {},
        { dataSources },
        { user: { _id: 'abc' } },
      );
      expect(result).toEqual(100);
      expect(dataSources.users.getCurrentPoint).toHaveBeenCalledWith({
        _id: 'abc',
      });
    });
  });

  describe('Query.getActiveUsers', () => {
    it('should return the list of active users', async () => {
      // mock the data sources object
      const dataSources = {
        users: {
          getActiveUsers: jest
            .fn()
            .mockResolvedValue([{ _id: 'abc' }, { _id: 'def' }]),
        },
      };

      const result = await users.Query.getActiveUsers(
        null,
        {},
        { dataSources },
      );
      expect(result).toEqual([{ _id: 'abc' }, { _id: 'def' }]);
      expect(dataSources.users.getActiveUsers).toHaveBeenCalled();
    });
  });
});
