import uuidv4 from 'uuid/v4';

export default function createUser(username, id) {
  return {
    _id: id || uuidv4(),
    username,
    gamerooms: [],
  };
}
