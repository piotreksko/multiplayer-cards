import { withFilter } from 'graphql-subscriptions';
import pubsub from '../pubsub';
import {
  PUSH_MESSAGE,
  UPDATE_GAMEROOM,
} from '../resolvers/common/subscriptionNames';

const gamerooms = {
  // gameProgress:  (
  //   { gameroomId },
  //   { context },
  //   { dataSources: { gameProgress } },
  // ) => {
  //   const progress  = gameProgress.getGameProgress(gameroom.gameProgressId)
  //   return progress;
  // },
  Query: {
    getGamerooms: (root, args, { dataSources: { gamerooms } }) => {
      return gamerooms.getGamerooms();
    },
    getGameroom: (root, { gameroomId }, { dataSources: { gamerooms } }) => {
      return gamerooms.getGameroom(gameroomId);
    },
  },
  Mutation: {
    createGameroom(root, { name }, { dataSources: { gamerooms }, user }) {
      const gameroom = gamerooms.createGameroom(name, user);
      // chatrooms.createChatroom(gameroom._id);
      return gameroom;
    },
    joinGameroom(root, { gameroomId }, { dataSources: { gamerooms }, user }) {
      gamerooms.joinGameroom(gameroomId, user);
      return null;
    },
    leaveGameroom(root, { gameroomId }, { dataSources: { gamerooms }, user }) {
      return gamerooms.leaveGameroom(gameroomId, user);
    },
    togglePlayerReady(
      root,
      { gameroomId },
      { dataSources: { gamerooms }, user },
    ) {
      return gamerooms.togglePlayerReady(gameroomId, user);
    },
    sendMessage(
      root,
      { gameroomId, message },
      { dataSources: { gamerooms }, user },
    ) {
      return gamerooms.sendMessage(gameroomId, message, user);
    },
    startGame(
      root,
      { gameroomId },
      { dataSources: { gamerooms, gameProgress }, user },
    ) {
      const gameroom = gamerooms.getGameroom(gameroomId);
      if (gameroom.host._id !== user._id) return false;
      const previousProgress =
        gameProgress.getGameProgress(gameroom.gameProgressId) || [];
      const progress = gameProgress.startGame(
        gameroom,
        previousProgress.winners,
        user,
      );
      gamerooms.assignGameProgress(gameroom, progress._id);
      return true;
    },
  },
  Subscription: {
    updateGameroom: {
      subscribe: withFilter(
        () => pubsub.asyncIterator([UPDATE_GAMEROOM]),
        (payload, args) => {
          return payload.updateGameroom._id === args.gameroomId;
        },
      ),
    },
    // newMessage: {
    //   subscribe: withFilter(
    //     () => pubsub.asyncIterator([PUSH_MESSAGE]),
    //     (payload, args) => {
    //       return payload.gameroomId === args.gameroomId;
    //     },
    //   ),
    // },
  },
};

export default gamerooms;
