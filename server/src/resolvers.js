// const data = {
//   gameProgress: [],
//   activeUsers: [...exampleUsers]
// };

const merge = require('lodash/merge');
import gameProgress from '@/resolvers/gameProgress';
import gamerooms from '@/resolvers/gamerooms';
import users from '@/resolvers/users';

exports.resolvers = merge(gamerooms, gameProgress, users);
