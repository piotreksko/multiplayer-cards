export const cardRanks = {
  2: "2",
  ACE: "ACE",
  KING: "KING",
  QUEEN: "QUEEN",
  JACK: "JACK",
  10: "10",
  9: "9",
  8: "8",
  7: "7",
  6: "6",
  5: "5",
  4: "4",
  3: "3",
};

export const CardRank = {
  TWO: "2",
  ACE: "ACE",
  KING: "KING",
  QUEEN: "QUEEN",
  JACK: "JACK",
  TEN: "10",
  NINE: "9",
  EIGHT: "8",
  SEVEN: "7",
  SIX: "6",
  FIVE: "5",
  FOUR: "4",
  THREE: "3",
};

export const CardSuit = {
  HEARTS,
  DIAMONDS,
  SPADES,
  CLUBS,
};

export const rankValues = {
  2: 15,
  ACE: 14,
  KING: 13,
  QUEEN: 12,
  JACK: 11,
  10: 10,
  9: 9,
  8: 8,
  7: 7,
  6: 6,
  5: 5,
  4: 4,
  3: 3,
};

export const cardRanksArray = [
  cardRanks[3],
  cardRanks[4],
  cardRanks[5],
  cardRanks[6],
  cardRanks[7],
  cardRanks[8],
  cardRanks[9],
  cardRanks[10],
  cardRanks.JACK,
  cardRanks.QUEEN,
  cardRanks.KING,
  cardRanks.ACE,
  cardRanks[2],
];

export const HEARTS = "HEARTS";
export const DIAMONDS = "DIAMONDS";
export const SPADES = "SPADES";
export const CLUBS = "CLUBS";

export const cardSuits = {
  HEARTS,
  DIAMONDS,
  SPADES,
  CLUBS,
};

export const cardSuitsArray = Object.values(cardSuits).map((suit) => suit);
export const errorCodes = {
  USERNAME_TAKEN: "Username is already taken. Please choose another one",
};
