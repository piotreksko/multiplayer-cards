const { Database } = require('./Database');

import typeDefs from './src/schemas/types/schema.graphql';

const { makeExecutableSchema } = require('graphql-tools');
const { resolvers } = require('./src/resolvers');
const { ApolloServer } = require('apollo-server-express');
const { UsersApi } = require('./src/dataSources/UsersApi');
const { GameroomsApi } = require('./src/dataSources/GameroomsApi');
const { GameProgressApi } = require('./src/dataSources/GameProgressApi');

// const jwt = require('jsonwebtoken');
// ObjectID = require('mongodb').ObjectID;

// const database = new Database();
// let connection = null;
// const connect = async () => {
//   connection = await database.connect();
// };
//
// connect();

const GameProgressInstance = new GameProgressApi();
const GameroomsInstance = new GameroomsApi();
const UsersApiInsance = new UsersApi();

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

const buildRequestContext = async ({
  req,
  connection,
  context,
  headers,
  payload,
}) => {
  if (connection) {
    return {
      ...connection.context,
    };
  } else {
    const { userid } = req.headers;
    const user = UsersApiInsance.getUser(userid);
    return { user };
  }
  const { userid } = req.headers;
  const user = UsersApiInsance.getUser(userid);
  return { user };
  // const context = {};

  // if (req.headers.userid && accessToken) {
  // if (reqHeaders || connectionHeaders) {
  // const user = await connection
  //   .collection('users')
  //   .find({ _id: ObjectID(req.headers.userid) })
  //   .toArray();
  // let tokenValid = false;
  // try {
  //   tokenValid = !!jwt.verify(accessToken, process.env.JWT_SECRET);
  // } catch (e) {
  //   // Token is invalid - nothing happens.
  // }
  // if (tokenValid && user) {
  //   context.user = user[0];
  // }
  // context.user = {};
  // }
  // return context;
};

const buildSubscriptions = () => ({
  onConnect: async (connectionParams = {}, webSocket) => {
    if (connectionParams) {
      const { userid } = connectionParams.headers;
      const user = UsersApiInsance.getUser(userid);
      return { user };
    }
    // console.log(webSocket.upgradeReq.headers);
    // const token = webSocket.upgradeReq.headers.authorization;
    // const user = getUserByToken(token);
    // return { user }; // this is connection.context above
  },
  onDisconnect: (webSocket, context) => {
    //TODO logout / setOffline on disconnect
  },
});

const cors = {
  origin: '*',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  preflightContinue: false,
  optionsSuccessStatus: 204,
};

const createApolloServer = (context = buildRequestContext) =>
  new ApolloServer({
    cors,
    context,
    schema,
    dataSources: () => ({
      users: UsersApiInsance,
      gamerooms: GameroomsInstance,
      gameProgress: GameProgressInstance,
    }),
    subscriptions: buildSubscriptions(),
  });

module.exports = { createApolloServer };
