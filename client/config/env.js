const path = require('path');

require('dotenv').config({ path: path.resolve(__dirname, '../.env') });

module.exports = {
  NODE_ENV: process.env.NODE_ENV || 'test',
  PUBLIC_URL: process.env.PUBLIC_URL || ''
};
