const path = require(`path`);

module.exports = {
  webpack: {
    alias: {
      '@': path.resolve(__dirname, 'src/'),
      apollo: path.resolve(__dirname, 'src/apollo'),
      '@components': path.resolve(__dirname, 'src/components'),
      '@context': path.resolve(__dirname, 'src/context'),
      '@constants': path.resolve(__dirname, 'src/constants'),
      '@hooks': path.resolve(__dirname, 'src/hooks'),
      '@scripts': path.resolve(__dirname, 'src/scripts'),
      '@utility': path.resolve(__dirname, 'src/utility'),
      '@views': path.resolve(__dirname, 'src/views'),
      types: path.resolve(__dirname, 'src/types')
    }
  },
  eslint: {
    enable: false
  }
};
