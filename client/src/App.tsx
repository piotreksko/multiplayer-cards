import React, { useState } from 'react';
import { ApolloProvider as ApolloHooksProvider } from '@apollo/react-hooks';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import { ApolloLink, from, split } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import AppContext from '@context/AppContext';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch
} from 'react-router-dom';
import { routes } from './constants/routes';
import GameroomWrapper from './views/Gameroom/GameroomWrapper';
import './style/style.scss';
import LogIn from '@views/LogIn/LogIn';
import GameroomsList from '@views/GameroomsList/GameroomsList';

const Root = () => (
  <Router>
    <Switch>
      <Route exact path={routes.GAMEROOMS} render={() => <GameroomsList />} />
      <Route exact path={routes.GAMEROOM} render={() => <GameroomWrapper />} />
      <Route exact path={routes.LOGIN} render={() => <LogIn />} />

      <Redirect to={routes.LOGIN} />
    </Switch>
  </Router>
);

// eslint-disable-next-line react/no-multi-comp
const App = () => {
  const userJson = localStorage.getItem('user');
  const currentUser = userJson ? JSON.parse(userJson) : null;
  const [user, setContextUser] = useState(currentUser);

  const setUser = (userInput: { _id: string; username: string }) => {
    localStorage.setItem('user', JSON.stringify(userInput));
    setContextUser(userInput);
  };

  const context = { user, setUser };

  const isDev = process.env.NODE_ENV === 'development';
  const uri = isDev
    ? 'localhost:4000/graphql'
    : 'cards-react-graphql-server.herokuapp.com/graphql';

  const wsUri = `ws://${uri}`;

  const httpLink = new HttpLink({
    uri: `http://${uri}`
  });

  const wsLink = () => {
    return new WebSocketLink({
      // cors: {
      //   credentials: true,
      //   origin: true
      // },
      uri: wsUri,
      options: {
        reconnect: true,
        connectionParams: {
          headers: {
            username: user ? user.username : null,
            userid: user ? user._id : null,
            authorization: user ? user.token : null
          }
        }
      }
    });
  };

  const authLink = new ApolloLink((operation, forward) => {
    if (user) {
      operation.setContext({
        headers: {
          username: user ? user.username : null,
          userid: user ? user._id : null,
          authorization: user ? user.token : null
        }
      });
    }

    return forward(operation);
  });

  const terminatingLink = split(
    ({ query }) => {
      const { kind, operation }: { kind: string; operation?: string } =
        getMainDefinition(query);
      return kind === 'OperationDefinition' && operation === 'subscription';
    },
    wsLink(),
    authLink.concat(httpLink)
  );

  const link = from([terminatingLink]);

  const cache = new InMemoryCache();

  const apolloClient = new ApolloClient({
    link: link as any,
    cache
  });

  return (
    <AppContext.Provider value={context}>
      {/*
        // @ts-ignore */}
      <ApolloProvider client={apolloClient}>
        {/*
        // @ts-ignore */}
        <ApolloHooksProvider client={apolloClient}>
          <Root />
        </ApolloHooksProvider>
      </ApolloProvider>
    </AppContext.Provider>
  );
};

export default App;
