import { createContext, useContext } from 'react';
import { UserLogIn } from 'types/User';

interface AppContextType {
  user: UserLogIn;
  setUser: ((user: UserLogIn) => void) | null;
}

const AppContext = createContext<AppContextType>({
  user: {} as UserLogIn,
  setUser: null
});

export const useAppContext = () => {
  return useContext(AppContext);
};

export default AppContext;
