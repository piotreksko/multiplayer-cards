import { ErrorCode, GraphqlError } from 'types/Error';

export const getErrorCode = (error: GraphqlError) => {
  const errorMessage =
    error.graphQLErrors?.[0] &&
    ErrorCode[error.graphQLErrors[0]?.message as keyof typeof ErrorCode];
  if (errorMessage) {
    console.log(errorMessage);
    return errorMessage;
  }
  console.log(error.message);
  return error.message;
};
