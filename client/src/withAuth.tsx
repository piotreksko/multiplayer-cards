import React from 'react';
import { Redirect, RouteComponentProps } from 'react-router-dom';
import { useAppContext } from '@context/AppContext';

interface Props {
  [key: string]: any;
}

function withAuth<T extends Props>(
  Component: React.ComponentType<T>
): React.FC<T & RouteComponentProps> {
  const WrappedComponent: React.FC<T & RouteComponentProps> = (props) => {
    const { user } = useAppContext();
    return user ? <Component {...props} /> : <Redirect to="/" />;
  };
  return WrappedComponent;
}

export default withAuth;
