import { rankValues } from '@constants/cards';
import { checkUniqueRanks } from '@hooks/usePlayerCardsWithProps';
import { Card, CardRank } from 'types/Card';
import { GameProgress } from 'types/Gameroom';

export function cardAmountsByRank(cards: Card[]): number[] {
  const amountOfEach = cards.reduce(
    (prev: { [key in CardRank]?: number }, curr) => {
      const obj: { [key: string]: number } = { ...prev };
      if (!prev[curr.rank]) {
        obj[curr.rank] = 1;
      } else {
        obj[curr.rank] += 1;
      }
      return obj;
    },
    {}
  );
  const cardsAmounts = [...new Set<number>(Object.values(amountOfEach))];
  return cardsAmounts;
}

export function canFinishWithStairs(
  cards: Card[],
  progress: GameProgress,
  userid: string
) {
  const uniqueRanks = checkUniqueRanks(cards);
  if (uniqueRanks.length < 3) return false;
  const steps = uniqueRanks.map((rank: CardRank) => rankValues[rank]);
  const stepDifferences = steps.map((step: number, index: number) => {
    const nextStep = steps[index + 1];
    return nextStep ? step - nextStep : 0;
  });
  stepDifferences.pop();
  const uniqueSteps = [...new Set(stepDifferences)];
  const playerStartsRound =
    !progress.pile.length || progress.lastPlayerId === userid;
  const canWeakestCardBeUsed =
    playerStartsRound || cards[cards.length - 1].available;

  return (
    canWeakestCardBeUsed &&
    uniqueSteps.length === 1 &&
    cards.length > 2 &&
    cardAmountsByRank(cards).length === 1
  );
}

export default {};
