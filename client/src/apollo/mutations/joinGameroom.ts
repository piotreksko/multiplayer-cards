import gql from 'graphql-tag';

const joinGameroom = gql`
  mutation ($gameroomId: ID!) {
    joinGameroom(gameroomId: $gameroomId)
  }
`;

export default joinGameroom;
