import { gql } from 'graphql-tag';

const togglePlayerReady = gql`
  mutation ($gameroomId: ID!) {
    togglePlayerReady(gameroomId: $gameroomId)
  }
`;

export default togglePlayerReady;
