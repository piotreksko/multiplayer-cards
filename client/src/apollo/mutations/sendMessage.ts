import gql from 'graphql-tag';

const sendMessage = gql`
  mutation ($gameroomId: ID!, $message: String!) {
    sendMessage(gameroomId: $gameroomId, message: $message) {
      _id
      text
      createdBy
      createdAt
    }
  }
`;
export default sendMessage;
