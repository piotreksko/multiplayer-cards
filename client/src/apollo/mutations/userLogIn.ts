import { gql } from 'graphql-tag';

const userLogin = gql`
  mutation ($username: String!) {
    userLogIn(username: $username) {
      _id
      username
    }
  }
`;

export default userLogin;
