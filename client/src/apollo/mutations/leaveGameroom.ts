import gql from 'graphql-tag';

const leaveGameroom = gql`
  mutation ($gameroomId: ID!) {
    leaveGameroom(gameroomId: $gameroomId)
  }
`;

export default leaveGameroom;
