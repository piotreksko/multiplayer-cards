import { gql } from 'graphql-tag';

const createGameroom = gql`
  mutation ($name: String!) {
    createGameroom(name: $name) {
      _id
    }
  }
`;

export default createGameroom;
