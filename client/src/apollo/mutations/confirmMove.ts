import { gql } from 'graphql-tag';

const confirmMove = gql`
  mutation ($gameProgressId: ID!, $cardIds: [ID!]) {
    confirmMove(gameProgressId: $gameProgressId, cardIds: $cardIds)
  }
`;

export default confirmMove;
