import gql from 'graphql-tag';

const passMove = gql`
  mutation ($gameProgressId: ID!) {
    passMove(gameProgressId: $gameProgressId)
  }
`;

export default passMove;
