import gql from 'graphql-tag';

const startGame = gql`
  mutation ($gameroomId: ID!) {
    startGame(gameroomId: $gameroomId)
  }
`;

export default startGame;
