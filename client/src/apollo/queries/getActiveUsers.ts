import gql from 'graphql-tag';

const getActiveUsers = gql`
  query {
    getActiveUsers {
      _id
      username
    }
  }
`;

export default getActiveUsers;
