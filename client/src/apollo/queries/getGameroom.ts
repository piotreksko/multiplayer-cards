import gql from 'graphql-tag';

const getGameroom = gql`
  query ($gameroomId: ID!) {
    getGameroom(gameroomId: $gameroomId) {
      _id
      name
      host {
        _id
        username
      }
      players {
        _id
        username
        isReady
      }
      gameInProgress
      gameProgressId
    }
  }
`;

export default getGameroom;
