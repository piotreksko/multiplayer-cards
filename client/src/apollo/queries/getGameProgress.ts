import gql from 'graphql-tag';

const getGameProgress = gql`
  query ($gameProgressId: ID!) {
    getGameProgress(gameProgressId: $gameProgressId) {
      _id
      players {
        _id
        username
        cards {
          _id
          rank
          suit
        }
        cardsCount
        hasToSwap
        swapsBestCards
        cardsToSwapCount
      }
      pile {
        _id
        rank
        suit
      }
      lastThreeTurns {
        cards {
          _id
          rank
          suit
        }
        isNewRound
      }
      lastPlayerId
      nextPlayerId
      stairsActivated
      stairsStep
      allowedCardsAmount
      lastGameWinners {
        _id
        username
      }
      winners {
        _id
        username
      }
      isNewRound
      isGameOver
      grandmaster
      cardsSwapDone
    }
  }
`;

export default getGameProgress;
