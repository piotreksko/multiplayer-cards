import gql from 'graphql-tag';

const getGamerooms = gql`
  query {
    getGamerooms {
      _id
      name
      players {
        _id
        username
      }
    }
  }
`;

export default getGamerooms;
