import gql from 'graphql-tag';

const updateGameProgress = gql`
  subscription ($gameProgressId: ID!) {
    updateGameProgress(gameProgressId: $gameProgressId) {
      players {
        _id
        username
        cards {
          _id
          rank
          suit
          available
        }
        cardsCount
        hasToSwap
        swapsBestCards
        cardsToSwapCount
      }
      pile {
        _id
        rank
        suit
      }
      lastThreeTurns {
        cards {
          _id
          rank
          suit
        }
        isNewRound
      }
      lastPlayerId
      nextPlayerId
      stairsActivated
      stairsStep
      allowedCardsAmount
      lastGameWinners {
        _id
        username
      }
      winners {
        _id
        username
      }
      isNewRound
      isGameOver
      grandmaster
      cardsSwapDone
    }
  }
`;

export default updateGameProgress;
