import gql from 'graphql-tag';

const updateGameroom = gql`
  subscription ($gameroomId: ID!) {
    updateGameroom(gameroomId: $gameroomId) {
      _id
      name
      host {
        _id
        username
      }
      players {
        _id
        username
        isReady
      }
      gameInProgress
      gameProgressId
    }
  }
`;

export default updateGameroom;
