import gql from 'graphql-tag';

const updateActiveUsers = gql`
  subscription {
    updateActiveUsers {
      _id
      username
    }
  }
`;

export default updateActiveUsers;
