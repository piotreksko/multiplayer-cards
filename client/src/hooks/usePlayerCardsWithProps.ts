/* eslint-disable max-lines */
import {
  cardRanksArray,
  HIGHEST_CARD_RANK,
  rankValues
} from '@constants/cards';
import { useAppContext } from '@context/AppContext';
import { Card, CardRank, CardSuit } from 'types/Card';
import { GameProgress } from 'types/Gameroom';
import { Player } from 'types/Player';

export function checkUniqueRanks(selectedCards: Card[]) {
  return [...new Set(selectedCards.map((card: Card) => card.rank))];
}

function checkEnoughRankCards(selectedCards: Card[], progress: GameProgress) {
  const uniqueRanks = checkUniqueRanks(selectedCards);
  const lastRank = uniqueRanks[uniqueRanks.length - 1];
  const cardsWithLastRank = selectedCards.filter(
    ({ rank }) => rank === lastRank
  );
  const enoughRankCards =
    cardsWithLastRank.length === progress.allowedCardsAmount;
  return enoughRankCards;
}

function lastRankIsLessThanFirst(
  uniqueRanks: CardRank[],
  selectedCards: Card[]
) {
  const firstRank = uniqueRanks[0];
  const lastRank = uniqueRanks[uniqueRanks.length - 1];
  const firstRankCards = selectedCards.filter(
    (card: Card) => card.rank === firstRank
  );
  const lastRankCards = selectedCards.filter(
    (card: Card) => card.rank === lastRank
  );
  return lastRankCards.length < firstRankCards.length;
}

function markSecondStep(
  uniqueRanks: CardRank[],
  cardsToReturn: Card[],
  markNextStep: (
    step: number,
    checkTwoSteps: boolean,
    toReturn: Card[]
  ) => Card[],
  progress: GameProgress,
  playerStartsRound: boolean
) {
  let toReturn = cardsToReturn;
  const highestDifference = HIGHEST_CARD_RANK - rankValues[uniqueRanks[0]];
  const highestPossibleStep = Math.trunc(highestDifference / 2);

  if (progress.stairsActivated && !playerStartsRound) {
    toReturn = markNextStep(progress.stairsStep, true, toReturn);
  } else {
    for (let i = 1; i < highestPossibleStep + 1; i += 1) {
      toReturn = markNextStep(i, true, toReturn);
    }
  }
  return toReturn;
}

function markThirdStep(
  uniqueRanks: CardRank[],
  selectedCards: Card[],
  cardsToReturn: Card[],
  markCardsByRank: (
    key: 'available' | 'possible' | 'selected',
    cardsToMark: Card[],
    rank: CardRank
  ) => Card[],
  markNextStep: (
    step: number,
    checkTwoSteps: boolean,
    toReturn: Card[]
  ) => Card[]
) {
  const lastRank = uniqueRanks[uniqueRanks.length - 1];

  if (lastRankIsLessThanFirst(uniqueRanks, selectedCards)) {
    return markCardsByRank('possible', cardsToReturn, lastRank);
  }
  const step = rankValues[lastRank] - rankValues[uniqueRanks[0]];
  return markNextStep(
    step,
    // unverified
    false,
    []
  );
}

const isCardHigher = (
  cardRank: CardRank,
  lastThreeTurns: { cards: Card[] }[]
) => {
  const lastTurn = lastThreeTurns[lastThreeTurns.length - 1];
  const lastTurnRank =
    (lastTurn &&
      lastTurn.cards &&
      lastTurn.cards[lastTurn.cards.length - 1].rank) ||
    0;
  return (
    cardRanksArray.indexOf(cardRank) - cardRanksArray.indexOf(lastTurnRank) > 0
  );
};

const usePlayerCardsWithProps = (
  progress: GameProgress,
  selectedCards: Card[]
) => {
  const {
    user: { _id: userid }
  } = useAppContext();

  const player = progress.players.find(({ _id }) => _id === userid) as Player;
  const { cards, hasToSwap } = player as Player;

  const isPlayerTurn = progress.nextPlayerId === userid;
  const isVeryFirstRound =
    !progress.nextPlayerId && !progress.lastGameWinners.length;
  const playerStartsRound =
    !progress.pile.length || progress.lastPlayerId === userid;
  if (
    (!isPlayerTurn && !isVeryFirstRound && !hasToSwap) ||
    progress.isGameOver
  ) {
    return cards;
  }

  const markSelectedCards = (cardsToMark: Card[]) =>
    cardsToMark.map((card: Card) => ({
      ...card,
      selected: selectedCards.some(({ _id }) => _id === card._id)
    }));

  const wereTheyUsedLastTurn = (
    requiredRank: CardRank,
    requiredAmount: number
  ) => {
    const { lastThreeTurns } = progress;
    const lastTurn = lastThreeTurns[lastThreeTurns.length - 1];
    const lastCardsUsed = lastTurn && lastTurn.cards;
    if (!lastCardsUsed) {
      return false;
    }
    const cardsWithRankUsed = lastCardsUsed.filter(
      ({ rank }) => rank === requiredRank
    ).length;

    return cardsWithRankUsed === requiredAmount;
  };

  const wereThreeSixPlayed = wereTheyUsedLastTurn(CardRank.SIX, 3);
  const wereFourJacksPlayed = wereTheyUsedLastTurn(CardRank.JACK, 4);
  const wereFiveAcesPlayed = wereTheyUsedLastTurn(CardRank.ACE, 5);

  const hasSufficientCardsAmount = (rank: CardRank, amount: number) => {
    const possibleCards = cards?.filter((card: Card) => card.rank === rank);
    const hasSufficientAmount = possibleCards?.length || 0 >= amount;
    return hasSufficientAmount;
  };

  const markCardsByRank = (
    key: 'available' | 'selected' | 'possible',
    cardsToMark: Card[],
    rank: CardRank
  ) =>
    cardsToMark.map((card: Card) => ({
      ...card,
      [key]: card[key] || card.rank === rank
    }));

  const markCombo = (
    cardsToCheck: Card[],
    isPossible: boolean,
    requiredRank: CardRank,
    requiredAmount: number
  ) => {
    const requiredCardsLength = cardsToCheck.filter(
      ({ rank }) => rank === requiredRank
    ).length;
    const hasOtherRanksSelected =
      selectedCards.filter((card: Card) => card.rank !== requiredRank).length >
      0;
    if (requiredCardsLength >= requiredAmount && !hasOtherRanksSelected) {
      return markCardsByRank(
        isPossible ? 'possible' : 'available',
        cardsToCheck,
        requiredRank
      );
    }
    return cardsToCheck;
  };

  const markAllCombos = (cardsToCheck: Card[], isPossible: boolean) => {
    let comboCardsMarked = cardsToCheck;
    if (!wereThreeSixPlayed && !wereFourJacksPlayed && !wereFiveAcesPlayed) {
      comboCardsMarked = markCombo(
        comboCardsMarked,
        isPossible,
        CardRank.SIX,
        3
      );
    }
    if (!wereFourJacksPlayed && !wereFiveAcesPlayed) {
      comboCardsMarked = markCombo(
        comboCardsMarked,
        isPossible,
        CardRank.JACK,
        4
      );
    }
    if (!wereFiveAcesPlayed) {
      comboCardsMarked = markCombo(
        comboCardsMarked,
        isPossible,
        CardRank.ACE,
        5
      );
    }
    return comboCardsMarked;
  };

  const markThreeOfClubs = (cardsToCheck: Card[]) =>
    cardsToCheck.map((card: Card) => ({
      ...card,
      available: card.rank === CardRank.THREE && card.suit === CardSuit.CLUBS
    }));

  const markTwos = (cardsToCheck: Card[]) => {
    const { lastThreeTurns } = progress;
    if (!lastThreeTurns.length) return cardsToCheck;
    const lastTurnCards = lastThreeTurns[lastThreeTurns.length - 1].cards;

    if (!lastTurnCards || !lastTurnCards.length) return cardsToCheck;

    const wereTwosUsedLastTurn = lastTurnCards.some(
      (card: Card) => card.rank === CardRank.TWO
    );
    const redTwosUsedLastTurn = lastTurnCards.filter(
      ({ rank, suit }: { rank: CardRank; suit: CardSuit }) =>
        suit === CardSuit.HEARTS && rank === CardRank.TWO
    );
    const normalTwosUsedLastTurn = lastTurnCards.filter(
      ({ rank, suit }: { rank: CardRank; suit: CardSuit }) =>
        suit !== CardSuit.HEARTS && rank === CardRank.TWO
    );

    const allTwos = cards.filter(
      ({ rank }: { rank: CardRank }) => rank === CardRank.TWO
    );
    const redTwosAmount = allTwos.filter(
      ({ suit }: { suit: CardSuit }) => suit === CardSuit.HEARTS
    ).length;

    if (wereTwosUsedLastTurn) {
      if (!redTwosAmount) {
        return cardsToCheck;
      }
      if (
        lastTurnCards.length <= allTwos.length &&
        normalTwosUsedLastTurn &&
        redTwosUsedLastTurn.length < redTwosAmount
      ) {
        // Only when opponent used more than 1 normal two, you can beat him using a normal two + reds
        const canUseNormalTwo = normalTwosUsedLastTurn.length > 1;
        const isCardAvailable = (card: Card) => {
          if (card.available) {
            return true;
          }
          if (card.rank !== CardRank.TWO) {
            return false;
          }
          if (!canUseNormalTwo && card.suit !== CardSuit.HEARTS) {
            return false;
          }
          // only 2 of hearts should reach this return
          return true;
        };
        return cardsToCheck.map((card: Card) => ({
          ...card,
          available: isCardAvailable(card)
        }));
      }
      return cardsToCheck;
    }
    const normalTwosAmount = allTwos.filter(
      ({ suit }: { suit: CardSuit }) => suit !== CardSuit.HEARTS
    ).length;
    const combinedPower = redTwosAmount * 2 + allTwos.length - redTwosAmount;
    const hasSufficientCombinedPower =
      combinedPower >= lastTurnCards.length ||
      combinedPower >= progress.allowedCardsAmount;
    const canUseNormalTwo =
      hasSufficientCombinedPower ||
      normalTwosAmount >= progress.allowedCardsAmount;
    const canUseRedTwo =
      hasSufficientCombinedPower ||
      redTwosAmount * 2 >= progress.allowedCardsAmount;

    const isCardAvailable = (card: Card) => {
      if (card.available) {
        return true;
      }
      if (card.rank === CardRank.TWO) {
        if (card.suit === CardSuit.HEARTS) {
          return canUseRedTwo;
        }
        return canUseNormalTwo;
      }
      return false;
    };

    return cardsToCheck.map((card: Card) => ({
      ...card,
      available: isCardAvailable(card)
    }));
  };

  const markCardsToSwap = (cardsToCheck: Card[], isPossible: boolean) => {
    if (player.swapsBestCards) {
      const cardsToSwap = cardsToCheck.slice(0, player.cardsToSwapCount);

      return cardsToCheck.map((card: Card) => ({
        ...card,
        available:
          card.available || cardsToSwap.some(({ _id }) => _id === card._id)
      }));
    }
    return cardsToCheck.map((card: Card) => ({
      ...card,
      [isPossible ? 'possible' : 'available']: true
    }));
  };

  const markNormalCards = (cardsToCheck: Card[]) => {
    if (playerStartsRound && !isVeryFirstRound) {
      return cardsToCheck.map((card: Card) => ({ ...card, available: true }));
    }

    const { lastThreeTurns } = progress;
    if (progress.stairsActivated) {
      const { pile, stairsStep } = progress;
      const lastCardRank = pile[pile.length - 1].rank;

      const desiredValue =
        stairsStep + rankValues[lastCardRank.toUpperCase() as CardRank];
      const stepHigher =
        Object.entries(rankValues)
          .filter((entry) => entry[1] === desiredValue)
          .map((entry) => entry[0]) || [];
      const desiredRank = stepHigher[0] ? stepHigher[0] : '';

      if (!desiredRank) {
        return cardsToCheck;
      }

      const hasSufficientCards = hasSufficientCardsAmount(
        desiredRank as CardRank,
        progress.allowedCardsAmount
      );

      return cardsToCheck.map((card: Card) => ({
        ...card,
        available:
          (card.rank === desiredRank && hasSufficientCards) || card.available
      }));
    }

    return cardsToCheck.map((card: Card) => ({
      ...card,
      available:
        card.available ||
        (isCardHigher(card.rank, lastThreeTurns) &&
          hasSufficientCardsAmount(card.rank, progress.allowedCardsAmount))
    }));
  };

  const markStairwayToHeaven = (toCheck: Card[], isPossible: boolean) => {
    const uniqueRanks = checkUniqueRanks(toCheck);
    const steps = uniqueRanks.map((rank) => rankValues[rank]);
    const stepDifferences = steps.map((step, index) => {
      const nextStep = steps[index + 1];
      return nextStep ? step - nextStep : 0;
    });
    stepDifferences.pop();
    const uniqueSteps = [...new Set(stepDifferences)];

    const hasFullStairwayToHeaven =
      uniqueRanks.includes(CardRank.THREE) &&
      uniqueRanks.length > 11 &&
      uniqueRanks.length === toCheck.length;
    if (
      (uniqueRanks.length === toCheck.length &&
        toCheck.length > 3 &&
        uniqueSteps.length === 1) ||
      hasFullStairwayToHeaven
    ) {
      return toCheck.map((cardToCheck) => ({
        ...cardToCheck,
        [isPossible ? 'possible' : 'available']: true
      }));
    }
    return toCheck;
  };

  const markAvailableCards = () => {
    let cardsToReturn = cards;
    if (selectedCards.length > 0) {
      return cardsToReturn;
    }

    if (isVeryFirstRound) {
      return markThreeOfClubs(cardsToReturn);
    }

    if (player.hasToSwap) {
      return markCardsToSwap(
        cardsToReturn,
        // not verified
        false
      );
    }

    if (!progress.cardsSwapDone) {
      return cardsToReturn;
    }

    cardsToReturn = markAllCombos(
      cardsToReturn,
      // not verified
      false
    );

    const wasComboUsed =
      wereThreeSixPlayed || wereFourJacksPlayed || wereFiveAcesPlayed;
    const userStartsNewRound =
      (progress.lastPlayerId === userid || progress.nextPlayerId === userid) &&
      progress.isNewRound;

    if (!wasComboUsed || userStartsNewRound) {
      cardsToReturn = markNormalCards(cardsToReturn) as Card[];
      cardsToReturn = markTwos(cardsToReturn);
    }

    return cardsToReturn;
  };

  const markPossibleCards = (cardsToCheck: Card[]) => {
    let cardsToReturn = cardsToCheck;
    if (!selectedCards.length) {
      return cardsToReturn;
    }

    if (!progress.cardsSwapDone && player.hasToSwap) {
      if (player.cardsToSwapCount === 1) {
        return cardsToReturn;
      }
      if (player.swapsBestCards) {
        return markCardsToSwap(cardsToReturn, true);
      }
      return cardsToReturn.map((card: Card) => ({ ...card, possible: true }));
    }

    const selectedSameRank = !selectedCards.some(
      (card: Card) => card.rank !== selectedCards[0].rank
    );

    const enoughRankCards = checkEnoughRankCards(selectedCards, progress);

    if ((selectedSameRank && playerStartsRound) || !enoughRankCards) {
      const { rank } = selectedCards[0];
      cardsToReturn = markCardsByRank('possible', cardsToReturn, rank);
    }

    const markNextStep = (
      step: number,
      checkTwoSteps: boolean,
      toReturn: Card[] = cardsToReturn
    ): Card[] => {
      const uniqueRanks = checkUniqueRanks(selectedCards);
      const hasSameCardsAmounts =
        uniqueRanks.length === 2 &&
        selectedCards.filter((card: Card) => card.rank === uniqueRanks[0])
          .length ===
          selectedCards.filter((card: Card) => card.rank === uniqueRanks[1])
            .length;

      const baseRank =
        hasSameCardsAmounts && uniqueRanks.length === 2
          ? uniqueRanks[uniqueRanks.length - 1]
          : uniqueRanks[0];
      const desiredNextRankCardsAmount = playerStartsRound
        ? selectedCards.filter((card: Card) => card.rank === baseRank).length
        : progress.allowedCardsAmount;

      const desiredValue = rankValues[baseRank] + step;

      if (desiredValue > HIGHEST_CARD_RANK) {
        return toReturn;
      }
      const stepHigher =
        Object.entries(rankValues)
          .filter((entry) => entry[1] === desiredValue)
          .map((entry) => entry[0]) || [];
      const stepHigherRank = stepHigher[0] || '';

      const stepHigherCards = cards.filter(
        (card: Card) =>
          card.rank === stepHigherRank &&
          (playerStartsRound ||
            isCardHigher(card.rank, progress.lastThreeTurns))
      );

      const twoStepsHigher = checkTwoSteps
        ? Object.entries(rankValues)
            .filter((entry) => entry[1] === step * 2 + rankValues[baseRank])
            .map((entry) => entry[0])
        : [];

      const twoStepsHigherRank = twoStepsHigher[0] || '';

      const twoStepsHigherCards = checkTwoSteps
        ? cards.filter((card: Card) => card.rank === twoStepsHigherRank)
        : [];

      if (
        stepHigherCards.length >= desiredNextRankCardsAmount &&
        (!checkTwoSteps ||
          twoStepsHigherCards.length >= desiredNextRankCardsAmount)
      ) {
        return toReturn.map((card: Card) => ({
          ...card,
          possible: card.possible || card.rank === stepHigherRank
        }));
      }
      return toReturn;
    };

    const markPossibleStairs = () => {
      const uniqueRanks = checkUniqueRanks(selectedCards);
      const lastRank = uniqueRanks[uniqueRanks.length - 1];

      if (uniqueRanks.length === 3) {
        if (lastRankIsLessThanFirst(uniqueRanks, selectedCards)) {
          cardsToReturn = markCardsByRank('possible', cardsToReturn, lastRank);
        }
      }

      const hasSameCardsAmounts =
        uniqueRanks.length === 2 &&
        selectedCards.filter((card: Card) => card.rank === uniqueRanks[0])
          .length ===
          selectedCards.filter((card: Card) => card.rank === uniqueRanks[1])
            .length;

      if (
        uniqueRanks.length === 2 &&
        (progress.allowedCardsAmount === selectedCards.length / 2 ||
          (playerStartsRound && hasSameCardsAmounts))
      ) {
        cardsToReturn = markThirdStep(
          uniqueRanks,
          selectedCards,
          cardsToReturn,
          markCardsByRank,
          markNextStep
          // needs verification
          // playerStartsRound
        );
      }

      if (uniqueRanks.length === 2 && !hasSameCardsAmounts) {
        cardsToReturn = markCardsByRank('possible', cardsToReturn, lastRank);
      }

      if (
        uniqueRanks.length === 1 &&
        (progress.allowedCardsAmount === selectedCards.length ||
          playerStartsRound)
      ) {
        cardsToReturn = markSecondStep(
          uniqueRanks,
          cardsToReturn,
          markNextStep,
          progress,
          playerStartsRound
        );
      }
    };

    markPossibleStairs();
    cardsToReturn = markAllCombos(cardsToReturn, true);
    cardsToReturn = markStairwayToHeaven(
      cardsToReturn,
      // not verified
      false
    );
    return cardsToReturn;
  };

  const getCardsWithProps = () => {
    const availableMarked = markAvailableCards();
    const possibleMarked = markPossibleCards(availableMarked);
    const selectedMarked = markSelectedCards(possibleMarked);
    return selectedMarked;
  };

  return getCardsWithProps();
};

export default usePlayerCardsWithProps;
