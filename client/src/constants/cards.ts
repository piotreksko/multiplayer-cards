import { CardRank } from 'types/Card';

export const HIGHEST_CARD_RANK = 15;

export const cardRanks = {
  2: '2',
  ACE: 'ACE',
  KING: 'KING',
  QUEEN: 'QUEEN',
  JACK: 'JACK',
  10: '10',
  9: '9',
  8: '8',
  7: '7',
  6: '6',
  5: '5',
  4: '4',
  3: '3'
};

export const rankValues = {
  [CardRank.TWO]: 15,
  [CardRank.ACE]: 14,
  [CardRank.KING]: 13,
  [CardRank.QUEEN]: 12,
  [CardRank.JACK]: 11,
  [CardRank.TEN]: 10,
  [CardRank.NINE]: 9,
  [CardRank.EIGHT]: 8,
  [CardRank.SEVEN]: 7,
  [CardRank.SIX]: 6,
  [CardRank.FIVE]: 5,
  [CardRank.FOUR]: 4,
  [CardRank.THREE]: 3
};

export const cardRanksArray = [
  CardRank.THREE,
  CardRank.FOUR,
  CardRank.FIVE,
  CardRank.SIX,
  CardRank.SEVEN,
  CardRank.EIGHT,
  CardRank.NINE,
  CardRank.TEN,
  CardRank.JACK,
  CardRank.QUEEN,
  CardRank.KING,
  CardRank.ACE,
  CardRank.TWO
];

export const CLUBS = 'CLUBS';
export const DIAMONDS = 'DIAMONDS';
export const SPADES = 'SPADES';
export const HEARTS = 'HEARTS';

export const cardSuits = {
  HEARTS,
  DIAMONDS,
  SPADES,
  CLUBS
};

export const cardSuitsArray = Object.values(cardSuits).map((suit) => suit);
