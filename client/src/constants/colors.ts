export default {
  main: '#143a53',
  magnolia: '#f7f0f5',
  grey: '#7b8489',
  //   grey: '#91a3b0',

  neutral: '#009ffd',
  warning: '#f3b61f',
  positive: '#a29f15',
  green: '#619b8a',
  negative: '#7b2d26',
  black1: '#222a3c',
  black2: '#2e3136',
  red: '#ed254e',
  test: '#78d45b'
};
