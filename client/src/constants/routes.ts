export const getGameroomRoute = (gameroomId = ':gameroomId') =>
  `/gameroom/${gameroomId}`;

export const routes = {
  ROOT: '/',
  GAMEROOMS: '/gamerooms',
  GAMEROOM: getGameroomRoute(),
  AGAINSTCOMPUTER: '/against-computer',
  LOGIN: '/login'
};
