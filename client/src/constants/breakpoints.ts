export const size = {
  xs: '768px',
  sm: '921px',
  md: '1199px'
};

export const device = {
  xs: `(max-width: ${size.xs})`,
  sm: `(min-width: ${size.xs}) and (max-width: ${size.sm}) , screen and (max-height: 800px)`,
  md: `(min-width: ${size.sm}) and (max-width: ${size.md}) , screen and (min-height: 801px) and (max-height: 860px)`
};

export default { size, device };
