import breakpoints from '@constants/breakpoints';
import colors from '@constants/colors';
import styled from 'styled-components';
import { height, width } from './cards.constant';
import cardBack from '../../content/images/cards/card_back.png';

interface CardProps {
  cardImg?: string;
  available?: boolean;
  possible?: boolean;
  selected?: boolean;
  index: number;
}

const Card = styled.div<CardProps>`
  height: ${height}px;
  width: ${width}px;
  background-size: ${width}px ${height}px;
  background-repeat: no-repeat;
  transition: 0.1s ease-in-out;
  border-radius: 7px;
  box-shadow: 0px 0px 20px 3px rgba(48, 48, 48, 0.15);
  background-image: url(${({ cardImg }) => cardImg || cardBack});
  margin-left: ${({ index }) => (index === 0 ? 0 : `-${width - 22}px`)};

  z-index: 1;
  ${({ available }) =>
    available &&
    `
  &:hover {
    transition: 0.1s;
    transform: translateY(-1rem);
    cursor: pointer;
  }

  box-shadow: 0px 0px 3px 3px rgba(204, 255, 51, 0.7)
  // box-shadow: 0px 0px 3px 3px ${colors.test}70;
`}

  ${({ selected }) =>
    selected &&
    `
  &:hover {
    transform: translateY(-1.4rem);
  }

  transform: translateY(-1rem);
  cursor: pointer;

  box-shadow: 0px 0px 3px 3px rgba(80, 40, 225, 0.9);
  // box-shadow: 0px 0px 5px 5px ${colors.neutral}80;
`}

  ${({ selected, possible }) =>
    !selected &&
    possible &&
    `
  &:hover {
    transform: translateY(-1.4rem);
  }

  cursor: pointer;

  box-shadow: 0px 0px 3px 3px ${colors.neutral}80;
`} /* @media ${breakpoints.device.xs} {
    height: ${height * 0.75}px;
    width: ${width * 0.75}px;
    background-size: ${width * 0.75}px ${height * 0.75}px;
    margin-left: ${({ index }) =>
    index === 0 ? 0 : `-${width * 0.75 - 20 * 0.75}px`};
  }

  @media ${breakpoints.device.sm} {
    height: ${height * 0.8}px;
    width: ${width * 0.8}px;
    background-size: ${width * 0.8}px ${height * 0.8}px;
    margin-left: ${({ index }) =>
    index === 0 ? 0 : `-${width * 0.8 - 20 * 0.8}px`};
  }

  @media ${breakpoints.device.md} {
    height: ${height * 0.9}px;
    width: ${width * 0.9}px;
    background-size: ${width * 0.9}px ${height * 0.9}px;
    margin-left: ${({ index }) =>
    index === 0 ? 0 : `-${width * 0.9 - 20 * 0.9}px`};
  } */
`;

export default Card;
