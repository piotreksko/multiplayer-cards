import React from 'react';
import CardComponent from './Card.styled';
import { Card as CardInt } from 'types/Card';

interface Props {
  card?: Partial<CardInt>;
  clickOwnCard?: (card: CardInt) => void;
  index?: number;
  showBack?: boolean;
  fromPile?: boolean;
}

const Card = ({ card = {}, clickOwnCard, index, showBack }: Props) => {
  const {
    available = false,
    selected = false,
    possible = false,
    rank = '',
    suit = ''
  } = card;

  const fileName =
    !showBack && `${rank.toLowerCase()}_of_${suit.toLowerCase()}`;

  const cardImg =
    !showBack && require(`../../content/images/cards/${fileName}.png`).default;

  const onClick = () => {
    if (clickOwnCard && (available || possible || selected)) {
      clickOwnCard(card as CardInt);
    }
  };

  return (
    <CardComponent
      onClick={onClick}
      available={available}
      possible={possible}
      selected={selected}
      cardImg={cardImg}
      index={index as number}
      data-testid="card-component"
    />
  );
};

export default Card;
