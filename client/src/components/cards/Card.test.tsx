import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Card from './Card';
import { Card as CardInt, CardRank, CardSuit } from 'types/Card';

const mockCard: CardInt = {
  _id: 1,
  suit: CardSuit.DIAMONDS,
  rank: CardRank.ACE,
  available: true,
  selected: false,
  possible: true
};

describe('Card', () => {
  it('should render with correct props', () => {
    const { getByTestId } = render(<Card card={mockCard} />);
    const cardComponent = getByTestId('card-component');

    expect(cardComponent).toBeInTheDocument();
  });

  it('should call clickOwnCard function when clicked', () => {
    const mockClickOwnCard = jest.fn();
    const { getByTestId } = render(
      <Card card={mockCard} clickOwnCard={mockClickOwnCard} />
    );
    const cardComponent = getByTestId('card-component');

    fireEvent.click(cardComponent);

    expect(mockClickOwnCard).toHaveBeenCalledWith(mockCard);
  });

  it('should not call clickOwnCard function when clicked with incorrect props', () => {
    const mockClickOwnCard = jest.fn();
    const { getByTestId } = render(
      <Card card={mockCard} clickOwnCard={mockClickOwnCard} />
    );
    const cardComponent = getByTestId('card-component');

    fireEvent.click(cardComponent);

    expect(mockClickOwnCard).not.toHaveBeenCalledWith({
      ...mockCard,
      possible: false
    });
  });

  it('should not call clickOwnCard function when clicked with no function provided', () => {
    const { getByTestId } = render(<Card card={mockCard} />);
    const cardComponent = getByTestId('card-component');

    fireEvent.click(cardComponent);

    // should not throw error and click should do nothing
  });
});
