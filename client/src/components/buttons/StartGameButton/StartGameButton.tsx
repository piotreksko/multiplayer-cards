import React from 'react';
import './StartGameButton.scss';
import { useMutation } from '@apollo/react-hooks';
import { useParams } from 'react-router';
import startGameMutation from 'apollo/mutations/startGame';
import { useAppContext } from '@context/AppContext';
import TopButton from '@components/global/TopButton';
import start from 'content/images/start.png';
import colors from '@constants/colors';
import { User } from 'types/User';

interface Params {
  gameroomId: string;
}

const StartGameButton = ({ host }: { host: User }) => {
  const { gameroomId } = useParams<Params>();
  const { user } = useAppContext() || {};
  const userId = user?._id || '';

  const [startGame] = useMutation(startGameMutation, {
    variables: {
      gameroomId
    }
  });

  const onClick = () => {
    startGame();
  };

  const isPlayerHost = userId === host._id;
  return (
    <>
      {isPlayerHost && (
        <TopButton
          icon={start}
          onClick={onClick}
          color={colors.positive}
          testId="start-game-button"
        >
          Start game
        </TopButton>
      )}
    </>
  );
};

export default StartGameButton;
