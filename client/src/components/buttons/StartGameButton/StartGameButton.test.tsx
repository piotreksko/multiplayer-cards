import React from 'react';
import {
  render,
  fireEvent,
  screen,
  waitFor,
  getByTestId
} from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import { useAppContext } from '@context/AppContext';
import StartGameButton from './StartGameButton';
import startGameMutation from 'apollo/mutations/startGame';
import { BrowserRouter, MemoryRouter, Route } from 'react-router-dom';

jest.mock('@context/AppContext', () => ({
  useAppContext: jest.fn()
}));

const mockUseAppContext = useAppContext as jest.MockedFunction<
  typeof useAppContext
>;

const newData = jest.fn();
const setUser = jest.fn();

const mocks = [
  {
    request: {
      query: startGameMutation,
      variables: {
        gameroomId: '1234'
      }
    },
    newData,
    // newData: jest.fn(() => ({
    //   data: {
    //     startGame: true
    //   }
    // })),
    result: {
      data: {
        startGame: true
      }
    }
  }
];

describe('StartGameButton', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  const hostUser = {
    _id: '1',
    username: 'host',
    isReady: true
  };

  const nonHostUser = {
    _id: '2',
    username: 'user',
    isReady: true
  };

  const gameroomId = 'gameroomId';

  it('should render the Start game button when the user is the host', async () => {
    mockUseAppContext.mockReturnValue({ user: hostUser, setUser });
    render(
      <MockedProvider mocks={mocks}>
        <MemoryRouter initialEntries={[`/gameroom/${gameroomId}`]}>
          <Route path="/gameroom/:gameroomId">
            <StartGameButton host={hostUser} />
          </Route>
        </MemoryRouter>
      </MockedProvider>
    );

    const startGameButton = screen.getByRole('button');
    expect(startGameButton).toBeInTheDocument();

    fireEvent.click(startGameButton);

    // await waitFor(() => {
    //   expect(mocks[0].newData).toHaveBeenCalled();
    // });
  });

  it('should not render the Start game button when the user is not the host', async () => {
    mockUseAppContext.mockReturnValue({
      user: nonHostUser,
      setUser
    });
    render(
      <BrowserRouter>
        <MockedProvider mocks={mocks}>
          <StartGameButton host={hostUser} />
        </MockedProvider>
      </BrowserRouter>
    );

    const startGameButton = screen.queryByRole('button');
    expect(startGameButton).not.toBeInTheDocument();
  });
});
