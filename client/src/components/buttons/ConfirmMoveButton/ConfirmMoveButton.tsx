import colors from '@constants/colors';
import React from 'react';
import PropTypes from 'prop-types';
import Button from '../../global/Button';

interface Props {
  confirmMove: () => void;
  disabled: boolean;
}

const ConfirmMoveButton = ({ confirmMove, disabled }: Props) => {
  return (
    <Button onClick={confirmMove} disabled={disabled} color={colors.positive}>
      ✓
    </Button>
  );
};

export default ConfirmMoveButton;
