import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import ConfirmMoveButton from './ConfirmMoveButton';

describe('ConfirmMoveButton', () => {
  it('should render correctly', () => {
    const { getByText } = render(
      <ConfirmMoveButton confirmMove={() => {}} disabled={false} />
    );
    expect(getByText('✓')).toBeInTheDocument();
  });

  it('should call the confirmMove function when clicked', () => {
    const confirmMoveMock = jest.fn();
    const { getByText } = render(
      <ConfirmMoveButton confirmMove={confirmMoveMock} disabled={false} />
    );
    fireEvent.click(getByText('✓'));
    expect(confirmMoveMock).toHaveBeenCalled();
  });

  it('should be disabled when disabled prop is true', () => {
    const { getByRole } = render(
      <ConfirmMoveButton confirmMove={() => {}} disabled={true} />
    );
    const button = getByRole('button');
    expect(button).toHaveAttribute('disabled');
  });
});
