import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { MockedProvider, MockedResponse } from '@apollo/client/testing';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import joinGameroomMutation from 'apollo/mutations/joinGameroom';
import JoinGameButton from './JoinGameButton';
import { getGameroomRoute } from '@constants/routes';

describe('JoinGameButton', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  const gameroomId = '123';

  const joinGameroomMock: MockedResponse = {
    request: {
      query: joinGameroomMutation,
      variables: { gameroomId }
    },
    result: {
      data: { joinGameroom: { id: gameroomId } }
    }
  };

  it('should render a button with text "Join"', () => {
    const { getByText } = render(
      <MockedProvider mocks={[joinGameroomMock]} addTypename={false}>
        <JoinGameButton gameroomId={gameroomId} />
      </MockedProvider>
    );
    expect(getByText('Join')).toBeInTheDocument();
  });

  it('should call joinGameroom mutation and redirect to gameroom page when button is clicked', async () => {
    const history = createMemoryHistory();
    history.push = jest.fn();

    const { getByText } = render(
      <MockedProvider mocks={[joinGameroomMock]} addTypename={false}>
        <Router history={history}>
          <JoinGameButton gameroomId={gameroomId} />
        </Router>
      </MockedProvider>
    );

    fireEvent.click(getByText('Join'));
    await waitFor(() => {
      expect(history.push).toHaveBeenCalledWith(getGameroomRoute(gameroomId));
    });
  });

  it('should not redirect if joinGameroom mutation is not successful', async () => {
    const history = createMemoryHistory();
    history.push = jest.fn();

    const { getByText } = render(
      <MockedProvider
        mocks={[joinGameroomMock]}
        addTypename={false}
        defaultOptions={{
          mutate: {
            errorPolicy: 'all'
          }
        }}
      >
        <Router history={history}>
          <JoinGameButton gameroomId={gameroomId} />
        </Router>
      </MockedProvider>
    );

    fireEvent.click(getByText('Join'));
    await waitFor(() => expect(history.push).not.toHaveBeenCalled());
  });
});
