import { getGameroomRoute } from '@constants/routes';
import React from 'react';
// import './JoinButton.scss';
import { MutationResult, useMutation } from '@apollo/react-hooks';
import { useHistory } from 'react-router-dom';
import joinGameroomMutation from 'apollo/mutations/joinGameroom';
import styled from 'styled-components';

interface Props {
  gameroomId: string;
}

const JoinGameButton = ({ gameroomId }: Props): JSX.Element => {
  const history = useHistory();

  const [joinGameroom] = useMutation(joinGameroomMutation, {
    variables: { gameroomId }
  });

  const handleJoinGameroom = async () => {
    const { loading, error } = (await joinGameroom()) as MutationResult;
    if (!loading && !error) {
      history.push(getGameroomRoute(gameroomId));
    }
  };

  return (
    <Button onClick={handleJoinGameroom} className={'join-button'}>
      Join
    </Button>
  );
};

const Button = styled.button`
  width: 8rem;
  height: 90%;
  float: right;
`;

export default JoinGameButton;
