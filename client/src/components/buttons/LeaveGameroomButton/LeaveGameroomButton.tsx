import React from 'react';
import { MutationResult, useMutation } from '@apollo/react-hooks';
import { useHistory, useParams } from 'react-router-dom';
import { routes } from '@constants/routes';
import leaveGameroomMutation from 'apollo/mutations/leaveGameroom';
import TopButton from '@components/global/TopButton';
import leave from 'content/images/leave.png';
import colors from '@constants/colors';

interface Params {
  gameroomId: string;
}

function LeaveGameroomButton() {
  const history = useHistory();
  const { gameroomId } = useParams<Params>();
  const [leaveGameroom] = useMutation(leaveGameroomMutation, {
    variables: {
      gameroomId
    }
  });

  const handleLeaveGameroom = async () => {
    const { loading, error } = (await leaveGameroom()) as MutationResult;

    if (!loading && !error) {
      history.push(routes.GAMEROOMS);
    }
  };

  return (
    <TopButton
      color={colors.warning}
      icon={leave}
      testId="leave-gameroom-button"
      onClick={handleLeaveGameroom}
    />
  );
}

export default LeaveGameroomButton;
