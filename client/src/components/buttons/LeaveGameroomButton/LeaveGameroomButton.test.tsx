import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { MemoryRouter, Route, Router } from 'react-router-dom';
import { MockedProvider, MockedResponse } from '@apollo/client/testing';
import leaveGameroomMutation from 'apollo/mutations/leaveGameroom';
import LeaveGameroomButton from './LeaveGameroomButton';
import { createMemoryHistory } from 'history';

describe('LeaveGameroomButton', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  const gameroomId = '1';

  it('should render without errors', () => {
    const { getByTestId } = render(
      <MockedProvider mocks={[]} addTypename={false}>
        <MemoryRouter initialEntries={[`/gameroom/${gameroomId}`]}>
          <Route path="/gameroom/:gameroomId">
            <LeaveGameroomButton />
          </Route>
        </MemoryRouter>
      </MockedProvider>
    );

    expect(getByTestId('leave-gameroom-button')).toBeInTheDocument();
  });

  it('should call the leaveGameroom mutation and redirect to gamerooms page', async () => {
    const mockLeaveGameroom = {
      request: {
        query: leaveGameroomMutation,
        variables: {
          gameroomId
        }
      },
      newData: jest.fn(() => ({
        data: {
          leaveGameroom: true
        }
      })),
      result: {
        data: {
          leaveGameroom: true
        }
      }
    };
    const history = createMemoryHistory();
    history.push(`/gameroom/${gameroomId}`);
    history.push = jest.fn();

    const { getByTestId } = render(
      <MockedProvider mocks={[mockLeaveGameroom]} addTypename={false}>
        <Router history={history}>
          <Route path={`/gameroom/:gameroomId`}>
            <LeaveGameroomButton />
          </Route>
        </Router>
      </MockedProvider>
    );

    fireEvent.click(getByTestId('leave-gameroom-button'));

    await waitFor(() => {
      expect(mockLeaveGameroom.newData).toHaveBeenCalled();
      expect(history.push).toHaveBeenCalledWith('/gamerooms');
    });
  });

  it('should call the leaveGameroom mutation and not redirect when request fails', async () => {
    const mockLeaveGameroom = {
      request: {
        query: leaveGameroomMutation,
        variables: {
          gameroomId
        }
      },
      newData: jest.fn(() => ({
        data: {
          leaveGameroom: true
        }
      })),
      result: {
        data: {
          leaveGameroom: true
        }
      }
    };
    const history = createMemoryHistory();
    history.push(`/gameroom/${gameroomId}`);
    history.push = jest.fn();

    const { getByTestId } = render(
      <MockedProvider
        mocks={[mockLeaveGameroom]}
        addTypename={false}
        defaultOptions={{
          mutate: {
            errorPolicy: 'all'
          }
        }}
      >
        <Router history={history}>
          <Route path={`/gameroom/:gameroomId`}>
            <LeaveGameroomButton />
          </Route>
        </Router>
      </MockedProvider>
    );

    fireEvent.click(getByTestId('leave-gameroom-button'));

    await waitFor(() => {
      expect(mockLeaveGameroom.newData).toHaveBeenCalled();
      expect(history.push).not.toHaveBeenCalled();
    });
  });
});
