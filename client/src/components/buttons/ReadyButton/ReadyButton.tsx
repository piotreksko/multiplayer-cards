import React from 'react';
import { useMutation } from '@apollo/react-hooks';
import { useParams } from 'react-router';
import togglePlayerReadyMutation from 'apollo/mutations/togglePlayerReady';
import { useAppContext } from '@context/AppContext';
import TopButton from '@components/global/TopButton';
import ready from 'content/images/ready.png';
import colors from '@constants/colors';
import { User } from 'types/User';

interface Params {
  gameroomId: string;
}

function ReadyButton({ players }: { players: User[] }) {
  const { user } = useAppContext() || { _id: '' };
  const { gameroomId } = useParams<Params>();

  const [togglePlayerReady] = useMutation(togglePlayerReadyMutation, {
    variables: { gameroomId }
  });
  const player = players.find(({ _id }) => _id === user?._id);
  const isPlayerReady = player ? player.isReady : false;
  const readyPlayersCount = players.filter(({ isReady }) => isReady).length;
  const maxReadyPlayersReached = readyPlayersCount === 4;
  const readyButtonEnabled = !maxReadyPlayersReached || isPlayerReady;

  const onClick = () => {
    if (readyButtonEnabled) togglePlayerReady();
  };

  return (
    <TopButton
      icon={ready}
      color={isPlayerReady ? colors.positive : colors.negative}
      onClick={onClick}
    />
  );
}

export default ReadyButton;
