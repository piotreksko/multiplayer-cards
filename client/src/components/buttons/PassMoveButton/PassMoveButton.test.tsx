import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import PassMoveButton from './PassMoveButton';
import passMoveMutation from 'apollo/mutations/passMove';

const mocks = [
  {
    request: {
      query: passMoveMutation,
      variables: {
        gameProgressId: '1234'
      }
    },
    result: {
      data: {
        passMove: true
      }
    }
  }
];

describe('PassMoveButton component', () => {
  it('should render the component with the correct props', () => {
    const { getByRole } = render(
      <MockedProvider mocks={mocks}>
        <PassMoveButton
          gameProgressId="1234"
          disabled={true}
          setSelectedCards={() => {}}
        />
      </MockedProvider>
    );
    const button = getByRole('button');
    expect(button).toBeInTheDocument();
    expect(button).toHaveAttribute('disabled');
    expect(button.textContent).toBe('PASS');
  });

  it('should call setSelectedCards and passMove when clicked', () => {
    const setSelectedCards = jest.fn();
    const { getByRole } = render(
      <MockedProvider mocks={mocks}>
        <PassMoveButton
          gameProgressId="1234"
          disabled={false}
          setSelectedCards={setSelectedCards}
        />
      </MockedProvider>
    );
    const button = getByRole('button');
    fireEvent.click(button);
    expect(setSelectedCards).toHaveBeenCalledWith([]);
  });
});
