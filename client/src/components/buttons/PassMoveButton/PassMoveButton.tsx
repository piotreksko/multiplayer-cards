import React from 'react';
import './PassMoveButton.scss';
import { useMutation } from '@apollo/react-hooks';
import PropTypes from 'prop-types';
import passMoveMutation from 'apollo/mutations/passMove';
import Button from '@components/global/Button';
import colors from '@constants/colors';
import { Card } from 'types/Card';

interface Props {
  gameProgressId: string;
  disabled: boolean;
  setSelectedCards: (cards: Card[]) => void;
}

const PassMoveButton = ({
  gameProgressId,
  disabled,
  setSelectedCards
}: Props) => {
  const [passMove] = useMutation(passMoveMutation, {
    variables: {
      gameProgressId
    }
  });

  const passMoveClick = () => {
    setSelectedCards([]);
    passMove();
  };

  return (
    <Button onClick={passMoveClick} disabled={disabled} color={colors.warning}>
      PASS
    </Button>
  );
};

PassMoveButton.propTypes = {
  gameProgressId: PropTypes.string,
  disabled: PropTypes.bool.isRequired
};

export default PassMoveButton;
