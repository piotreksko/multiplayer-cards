import colors from '@constants/colors';
import styled from 'styled-components';

export const buttonColors = {
  orange: 'orange',
  neutral: 'neutral'
};

interface Props {
  color: string;
  disabled: boolean;
}

const Button = styled.button<Props>`
  border-radius: 10px;
  width: 9rem;
  height: 4rem;
  margin: 1rem;
  border: 0;
  background: ${colors.black1};
  padding: 0;
  margin: 10px 15px 10px 10px;
  border: 4px solid ${(props) => props.color || colors.grey};
  color: white;
  /* box-shadow: 0px 0px 3px 3px rgba(80, 40, 225, 0.9); */
  box-shadow: 0px 0px 3px 3px ${(props) => props.color || colors.grey}80;
  font-weight: bold;

  ${({ disabled, color }) =>
    !disabled &&
    `
    &:hover {
      background: ${color}60;
      cursor: pointer;
    }
  `}

  ${({ disabled }) =>
    disabled &&
    `
      background: ${colors.black2};
      color: ${colors.grey};
      border: 1px solid ${colors.grey};
      box-shadow: none;
  `}
`;

export default Button;
