import colors from '@constants/colors';
import React from 'react';
import styled from 'styled-components';

interface ButtonProps {
  color?: string;
  disabled?: boolean;
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const Button = styled.button<ButtonProps>`
  border-radius: 5px;
  height: 42px;
  width: 42px;
  border: 0;
  background: ${colors.black1};
  padding: 0;
  margin: 10px 15px 10px 10px;
  border: 3px solid ${(props) => props.color || colors.grey};
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;

  &:hover {
    background: rgba(255, 255, 255, 0.2);
    cursor: pointer;
  }

  ${({ disabled }) => disabled && `background-color: ${colors.black2}`}
`;

interface IconProps {
  url: string;
}

const Icon = styled.div<IconProps>`
  display: flex;
  align-items: center;
  justify-content: center;
  background-image: url(${(props) => props.url});
  height: 24px;
  width: 24px;
  background-size: contain;
  background-repeat: no-repeat;
`;

interface TopButtonProps {
  icon: string;
  color?: string;
  disabled?: boolean;
  children?: string;
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
  testId?: string;
}

const TopButton: React.FC<TopButtonProps> = ({
  icon,
  color,
  disabled,
  testId,
  onClick
}) => {
  return (
    <Button
      onClick={onClick}
      color={color}
      disabled={disabled}
      data-testid={testId}
    >
      <Icon url={icon} />
    </Button>
  );
};

export default TopButton;
