import React, { useState } from 'react';
import './LogIn.scss';
import { useMutation } from '@apollo/react-hooks';
import { useHistory } from 'react-router-dom';
import { useAppContext } from '@context/AppContext';
import userLogIn from 'apollo/mutations/userLogIn';
import { routes } from '@constants/routes';
import { getErrorCode } from '@utility/utility';

const getRandomUsername = () =>
  `test${Math.floor(Math.random() * (10000 - 0 + 1)) + 0}`;

export const LogIn = () => {
  const history = useHistory();
  const { setUser } = useAppContext();
  const [username, setUsername] = useState(getRandomUsername());
  const [userLogInMutation] = useMutation(userLogIn);

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    try {
      const { data } = await userLogInMutation({ variables: { username } });
      setUser?.(data.userLogIn);
      history.push(routes.GAMEROOMS);
    } catch (error: any) {
      getErrorCode(error);
    }
  };

  const isFormValid = !!username;

  return (
    <div className="login-menu">
      <h1>Log in</h1>
      {/* <Row center="xs"> */}
      {/* <Col> */}
      <h2>Choose your username</h2>
      <form className="form" onSubmit={(e) => handleSubmit(e)}>
        <input
          type="text"
          name="username"
          placeholder="Username"
          value={username}
          onChange={(event) => setUsername(event.target.value)}
        />
        <button
          type="submit"
          className="button-primary"
          disabled={!isFormValid}
        >
          Log in
        </button>
      </form>
      {/* </Col> */}
      {/* </Row> */}
    </div>
  );
};

export default LogIn;
