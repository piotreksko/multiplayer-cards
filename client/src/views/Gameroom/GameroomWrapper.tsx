import React from 'react';
import { withRouter, useParams } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import withAuth from '../../withAuth';
import Gameroom from './Gameroom';
import updateGameroomSubscription from 'apollo/subscriptions/updateGameroom';
import getGameroomQuery from 'apollo/queries/getGameroom';

export const GameroomWrapper = () => {
  const { gameroomId } = useParams<{ gameroomId: string }>();

  const { data, loading, error, subscribeToMore } = useQuery(getGameroomQuery, {
    variables: {
      gameroomId
    }
  });

  if (loading) return <div>Loading</div>;
  if (error) return <div>Error</div>;

  subscribeToMore({
    document: updateGameroomSubscription,
    variables: { gameroomId },
    updateQuery: (prev = {}, { subscriptionData }) => {
      if (!subscriptionData.data.updateGameroom) return prev;
      return subscriptionData.data.updateGameroom;
    }
  });

  return (
    <Gameroom
      {...data.getGameroom}
      gameroomId={gameroomId}
      // subscribeGameroom={subscribeGameroom}
    />
  );
};

export default withRouter(withAuth(GameroomWrapper));
