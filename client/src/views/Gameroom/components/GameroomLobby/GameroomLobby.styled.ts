import { size } from '@constants/breakpoints';
import { FlexRow } from '@components/global/Flex';
import styled from 'styled-components';

export const Container = styled(FlexRow)`
  width: 100vw;
  max-width: ${size.sm};
  height: 10vh;
  background-color: rgba(0, 0, 0, 0.2);
  border-radius: 8px;
  padding: 1rem 0;
  margin-top: 1rem;
  color: #fff;
  min-height: 80px;
  justify-content: start;
  padding-left: 1rem;

  @media ${`(max-width: ${size.sm})`} {
    margin-top: 0;
  }

  @media screen and (max-height: 930px) {
    margin-top: 0;
  }
`;
