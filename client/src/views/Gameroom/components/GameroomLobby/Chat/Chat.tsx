import React, { ChangeEvent, ChangeEventHandler, MouseEvent, useState } from 'react';
import DisplayMessage from './ChatMessage/DisplayMessage';
import './Chat.scss';
import SendMessage from './ChatMessage/SendMessage';
import { Message } from 'types/Chat';

interface Props {
  messages: Message[]
}

function Chat({ messages }: Props) {
  const [message, setMessage] = useState('');

  const handleChange = (event: ChangeEvent<HTMLTextAreaElement>) => {
    setMessage(event.target.value);
  };

  return (
    <div className="chat">
      <div className="messages">
        <div className="messages-content">
          {messages?.map((msg) => (
            <DisplayMessage {...msg} key={msg._id} />
          ))}
        </div>
      </div>

      <div className="message-box">
        <textarea
          className="message-input"
          value={message}
          onChange={handleChange}
          placeholder="Type message..."
        />
        <SendMessage />
      </div>
    </div>
  );
}

export default Chat;
