import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { useAppContext } from '@context/AppContext';

interface Props {
  text: string;
  createdBy: string;
  createdAt: string;
}

function DisplayMessage({ text, createdBy, createdAt }: Props) {
  // const createdDate = moment(createdAt).format('HH:mm:ss');
  return (
    <div className="message new">
      <span>{text}</span>
      <div className="timestamp">{createdAt}</div>
      <div className="timestamp">{createdBy}</div>
    </div>
  );
}

DisplayMessage.propTypes = {
  createdAt: PropTypes.string,
  createdBy: PropTypes.string,
  text: PropTypes.string
};

export default DisplayMessage;
