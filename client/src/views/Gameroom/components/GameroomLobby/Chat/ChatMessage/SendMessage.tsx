import { useMutation } from '@apollo/react-hooks';
import React, { MouseEvent, useState } from 'react';
import PropTypes from 'prop-types';
import { useParams } from 'react-router';
import sendMessageMutation from 'apollo/mutations/sendMessage';

function SendMessage() {
  const [message, setMessage] = useState('');
  const { gameroomId } = useParams<{ gameroomId: string }>();

  const [sendMessage] = useMutation(sendMessageMutation, {
    variables: {
      gameroomId,
      message
    }
  });

  const handleSubmit = (e: MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();

    sendMessage().then(() => {
      setMessage('');
    });
  };

  return (
    <button type="submit" className="message-submit" onClick={handleSubmit}>
      Send
    </button>
  );
}

SendMessage.propTypes = {
  currentUser: PropTypes.object,
  gameroomId: PropTypes.string
};

export default SendMessage;
