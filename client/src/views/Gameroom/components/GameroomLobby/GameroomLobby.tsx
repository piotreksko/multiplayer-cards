import React from 'react';
import { Container } from './GameroomLobby.styled';
import LeaveGameroomButton from '@components/buttons/LeaveGameroomButton/LeaveGameroomButton';
import ReadyButton from '@components/buttons/ReadyButton/ReadyButton';
import StartGameButton from '@components/buttons/StartGameButton/StartGameButton';
import { GameProgress } from 'types/Gameroom';

interface Props {
  progress: GameProgress;
  gameInProgress: boolean;
}

const GameroomLobby = ({ progress, gameInProgress }: Props) => {
  const gameNotStarted = !gameInProgress || progress.isGameOver;
  return (
    <Container>
      {gameNotStarted && <StartGameButton host={progress.players[0]} />}
      <LeaveGameroomButton />
      {gameNotStarted && <ReadyButton players={progress.players} />}
    </Container>
  );
};

export default GameroomLobby;
