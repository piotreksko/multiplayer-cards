import React from 'react';
import { withRouter } from 'react-router-dom';
import { FlexCol, FlexRow } from '@components/global/Flex';
import withAuth from '../../withAuth';
import LeaveGameroomButton from '../../components/buttons/LeaveGameroomButton/LeaveGameroomButton';
import StartGameButton from '../../components/buttons/StartGameButton/StartGameButton';
import ReadyButton from '../../components/buttons/ReadyButton/ReadyButton';
// import Chat from '../../components/Chat/Chat';
import GameProgress from '../GameProgress/GameProgress';
import { Container } from './Gameroom.styled';
import { Message } from 'types/Chat';
import { User } from 'types/User';
import { Player } from 'types/Player';
import GameInfo from '@views/GameProgress/components/GameInfo/GameInfo';

interface Props {
  name: string;
  host: User;
  players: Player[];
  messages: Message[];
  gameProgressId: string;
  gameInProgress: boolean;
}

const Gameroom = (props: Props) => {
  const { name, host, players, messages, gameProgressId, gameInProgress } =
    props;

  return (
    <div>
      {gameInProgress && (
        <GameProgress
          gameProgressId={gameProgressId}
          gameInProgress={gameInProgress}
        />
      )}

      <Container>
        <FlexRow>
          <FlexCol>
            <StartGameButton host={host} />
            <LeaveGameroomButton />
            <ReadyButton players={players} />
          </FlexCol>
          <FlexRow>
            <GameInfo
              players={players}
              gameroomName={name}
              hostName={host.username}
            />
            {/* <Chat messages={messages} /> */}
          </FlexRow>
        </FlexRow>
      </Container>
    </div>
  );
};

export default withRouter(withAuth(Gameroom));
