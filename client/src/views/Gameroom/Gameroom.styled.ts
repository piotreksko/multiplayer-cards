import styled from 'styled-components';

export const Container = styled.div`
  font-family: 'Almendra', serif;
  background-color: #f8f5de;
  background-image: linear-gradient(
    to right,
    rgba(255, 210, 0, 0.4),
    rgba(200, 160, 0, 0.1) 11%,
    rgba(0, 0, 0, 0) 35%,
    rgba(200, 160, 0, 0.1) 65%
  );
  text-align: justify;
  text-justify: inter-word;
  font-family: 'Almendra', serif;
  z-index: 99999;
  padding: 10px;
  border: 2px solid black;
`;
