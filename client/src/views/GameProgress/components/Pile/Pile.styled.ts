import breakpoints from '@constants/breakpoints';
import { height, width } from '@components/cards/cards.constant';
import { FlexCol } from '@components/global/Flex';
import styled from 'styled-components';

const cardLeftProp = (props: { adjustPosition: number }, multiply: number) => {
  return `${width * multiply * 0.15 * props.adjustPosition}px`;
};

export const Wrapper = styled(FlexCol)`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const CardWrapper = styled.div<{
  adjustPosition: number;
  cardIndex: number;
}>`
  position: absolute;
  transform: ${({ adjustPosition }) => `rotate(${3 * adjustPosition}deg)`};
  left: ${(props) => cardLeftProp(props, 1)};
  top: ${(props) => cardLeftProp(props, 0.1)};

  /* @media ${breakpoints.device.xs} {
    left: ${(props) => cardLeftProp(props, 0.7)};
    top: ${(props) => cardLeftProp(props, 0.1)};
  }

  @media ${breakpoints.device.sm} {
    left: ${(props) => cardLeftProp(props, 0.8)};
    top: ${(props) => cardLeftProp(props, 0.1)};
  }

  @media ${breakpoints.device.md} {
    left: ${(props) => cardLeftProp(props, 0.9)};
    top: ${(props) => cardLeftProp(props, 0.1)};
  } */
`;

export const Container = styled(FlexCol)`
  justify-content: space-between;
  margin: 1rem 0;
  margin-left: ${width / 1.6}px;
  height: ${height}px;
  /* 
  @media ${breakpoints.device.xs} {
    height: ${height * 0.7}px;
  }

  @media ${breakpoints.device.sm} {
    height: ${height * 0.8}px;
  }

  @media ${breakpoints.device.md} {
    height: ${height * 0.9}px;
  } */
`;

export const MainCards = styled.div`
  position: relative;
`;
