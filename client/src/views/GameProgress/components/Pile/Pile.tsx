import React from 'react';
import PropTypes from 'prop-types';
import ProgressIndicators from '../ProgressIndicators/ProgressIndicators';
import { Wrapper, Container, MainCards, CardWrapper } from './Pile.styled';
import { GameProgress } from 'types/Gameroom';
import { Card as CardInt } from 'types/Card';
import Card from '@components/cards/Card';

interface Props {
  lastThreeTurns: { cards: CardInt[] }[];
  progress: GameProgress;
}

const Pile = ({ lastThreeTurns = [], progress }: Props) => {
  const allCards = lastThreeTurns
    .map((turn, turnIndex) =>
      turn.cards.map((card) => ({
        ...card,
        turnIndex
      }))
    )
    .flat();

  const adjust = (index: number) => {
    const base = index - (allCards.length / 2 - 0.5);
    // const turnSpace = card.turnIndex === 1 ? 1.3 : 1.3;
    const turnSpace = 1.3;
    return base ? base * turnSpace : 0;
  };

  const cardsToRender = allCards
    .map((card, index) => ({
      ...card,
      adjustPosition: adjust(index)
    }))
    .flat();

  return (
    <Wrapper>
      <Container>
        <MainCards>
          {cardsToRender.map((card, index) => (
            <CardWrapper
              key={card._id}
              cardIndex={index}
              adjustPosition={card.adjustPosition}
            >
              <Card card={card} fromPile />
            </CardWrapper>
          ))}
        </MainCards>
      </Container>
      <ProgressIndicators progress={progress} />
    </Wrapper>
  );
};

Pile.propTypes = {
  cards: PropTypes.array
};

export default Pile;
