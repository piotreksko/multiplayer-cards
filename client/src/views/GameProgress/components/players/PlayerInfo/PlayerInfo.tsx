import React from 'react';
import { BottomRow, Icon, Info } from './PlayerInfo.styled';
import star from 'content/images/star.png';
import { Player } from 'types/Player';

interface Props {
  player: Player;
  nextPlayerId: string;
  lastPlayerId: string;
}

const PlayerInfo = ({ player, nextPlayerId, lastPlayerId }: Props) => {
  const isNextPlayer = nextPlayerId === player._id;
  const isLastPlayer = lastPlayerId === player._id;

  return (
    <>
      <Info isNextPlayer={isNextPlayer}>{player.username}</Info>
      <BottomRow>{isLastPlayer && <Icon url={star} />}</BottomRow>
    </>
  );
};

export default PlayerInfo;
