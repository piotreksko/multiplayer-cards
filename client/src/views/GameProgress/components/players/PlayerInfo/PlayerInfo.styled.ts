import colors from '@constants/colors';
import { width } from '@components/cards/cards.constant';
import styled from 'styled-components';

interface Props {
  isNextPlayer: boolean;
}

export const Info = styled.div<Props>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-width: ${width}px;
  max-width: 140px;
  padding: 6px;
  background-color: ${(props) => props.isNextPlayer && 'rgba(0, 0, 0, 0.3)'};
  border: ${(props) => props.isNextPlayer && `3px solid ${colors.positive}`};
  box-shadow: ${(props) =>
    props.isNextPlayer && '0px 0px 10px 2px rgb(0 0 0 / 30%)'};
  color: rgba(255, 255, 255, 0.9);
  border-radius: 8px;
  font-family: 'Almendra', serif;
  font-size: 16px;
  margin-top: 12px;
`;

export const BottomRow = styled.div`
  display: flex;
  align-items: center;
  font-weight: bold;
  font-size: 18px;
`;

export const Icon = styled.div<{ url: string }>`
  display: flex;
  align-items: center;
  justify-content: center;
  background-image: url(${(props) => props.url});
  height: 24px;
  width: 24px;
  background-size: contain;
  background-repeat: no-repeat;
  margin-top: 3px;
  margin-left: 4px;
`;
