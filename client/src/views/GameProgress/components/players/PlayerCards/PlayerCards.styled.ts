import styled from 'styled-components';

interface Props {
  cardsAmount: number;
}

export const Container = styled.div<Props>`
  display: flex;
  margin-top: 1rem;
  justify-content: space-evenly;
  /* width: ${(props) => `calc(${props.cardsAmount}*1.5vh)`}; */
`;
