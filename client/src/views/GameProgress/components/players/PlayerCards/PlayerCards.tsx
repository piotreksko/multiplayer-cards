import { cardSuits, rankValues } from '@constants/cards';
import React from 'react';
import { useAppContext } from '@context/AppContext';
import usePlayerCardsWithProps from '@hooks/usePlayerCardsWithProps';
import { canFinishWithStairs } from '@scripts/gameLogic';
import { Container } from './PlayerCards.styled';
import { GameProgress } from 'types/Gameroom';
import { Card as CardInt, CardRank, CardSuit } from 'types/Card';
import { Player } from 'types/Player';
import Card from '@components/cards/Card';

interface Props {
  progress: GameProgress;
  selectedCards: CardInt[];
  setSelectedCards: (cards: CardInt[]) => void;
}

const PlayerCards = ({ progress, selectedCards, setSelectedCards }: Props) => {
  const {
    user: { _id: userid }
  } = useAppContext();

  const player =
    progress.players.find(({ _id }) => _id === userid) || ({} as Player);
  const { cards = [] } = player;

  const isVeryFirstRound =
    !progress.nextPlayerId && !progress.lastGameWinners.length;

  const deselectCard = (card: CardInt) => {
    const selectedWithoutCardAndHigher = selectedCards.filter(
      (selectedCard) =>
        rankValues[selectedCard.rank] < rankValues[card.rank] ||
        (selectedCard.rank === card.rank && selectedCard.suit !== card.suit)
    );

    if (
      isVeryFirstRound &&
      card.rank === CardRank.THREE &&
      card.suit === CardSuit.CLUBS
    ) {
      return setSelectedCards([]);
    }

    setSelectedCards(selectedWithoutCardAndHigher);
  };

  const swapCardClicked = (card: CardInt) => {
    const { cardsToSwapCount } = player;

    if (player.swapsBestCards) {
      const allAvailableCards = cards.slice(0, cardsToSwapCount);
      return setSelectedCards(allAvailableCards);
    }

    if (cardsToSwapCount < selectedCards.length) {
      return setSelectedCards([...selectedCards, card]);
    }
    return setSelectedCards([...selectedCards.slice(1), card]);
  };

  const clearHigherRanks = (card: CardInt) => {
    const firstRank = selectedCards[0].rank;
    const firstRankCards = selectedCards.filter(
      ({ rank }) => rank === firstRank
    );
    setSelectedCards([...firstRankCards, card]);
  };

  const selectCard = (card: CardInt) => {
    if (player.hasToSwap) {
      return swapCardClicked(card);
    }

    if (card.available && !card.possible) {
      setSelectedCards([card]);
    }

    if (canFinishWithStairs(cards, progress, userid)) {
      return setSelectedCards(cards);
    }

    const isLowerChosen =
      selectedCards.length &&
      rankValues[card.rank.toUpperCase() as CardRank] <
        rankValues[
          selectedCards[selectedCards.length - 1].rank.toUpperCase() as CardRank
        ];

    if (isLowerChosen) {
      return clearHigherRanks(card);
    }

    return setSelectedCards([...selectedCards, card]);
  };

  const clickOwnCard = (card: CardInt) => {
    const cardWasAlreadySelected = selectedCards.some(
      ({ _id }) => _id === card._id
    );

    if (cardWasAlreadySelected) {
      deselectCard(card);
    } else {
      selectCard(card);
    }
  };
  debugger;
  const cardsToRender = usePlayerCardsWithProps(progress, selectedCards);

  return (
    <Container cardsAmount={cardsToRender.length}>
      {cardsToRender.map((card: CardInt, index: number) => (
        <Card
          key={card._id}
          clickOwnCard={clickOwnCard}
          card={card}
          index={index}
        />
      ))}
    </Container>
  );
};

export default PlayerCards;
