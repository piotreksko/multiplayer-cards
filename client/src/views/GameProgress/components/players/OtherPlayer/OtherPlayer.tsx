import React from 'react';
import { FlexCol } from '@components/global/Flex';
import PlayerInfo from '../PlayerInfo/PlayerInfo';
import { Container, InfoWrapper } from './OtherPlayer.styled';
import { Player } from 'types/Player';
import { Card as CardInt } from 'types/Card';
import Card from '@components/cards/Card';

interface Props {
  player: Player;
  isGameOver: boolean;
  isLeftPlayer?: boolean;
  isRightPlayer?: boolean;
  nextPlayerId: string;
  lastPlayerId: string;
}

const OtherPlayer = ({
  player,
  isGameOver,
  isLeftPlayer,
  isRightPlayer,
  nextPlayerId,
  lastPlayerId
}: Props) => {
  const cardsToRender: JSX.Element[] = [];
  for (let i = 0; i < player.cardsCount; i += 1) {
    const cardToPush =
      isGameOver && player.cards ? (
        <Card key={player.cards[i]._id} card={player.cards[i]} />
      ) : (
        <Card key={i} index={i} showBack />
      );
    cardsToRender.push(cardToPush);
  }

  const cardsRotation = () => {
    if (isLeftPlayer) return '-90';
    if (isRightPlayer) return '90';
    return '';
  };

  const renderCards = () => {
    const component = (
      <Container
        rotation={cardsRotation()}
        isLeftPlayer={isLeftPlayer}
        isRightPlayer={isRightPlayer}
      >
        {cardsToRender.map((card) => card)}
      </Container>
    );

    return isLeftPlayer || isRightPlayer ? (
      <div style={{ display: 'table' }}>
        <div style={{ padding: '50% 0', height: 0 }}>{component}</div>
      </div>
    ) : (
      component
    );
  };

  return (
    <FlexCol>
      {renderCards()}
      <InfoWrapper>
        <PlayerInfo
          player={player}
          nextPlayerId={nextPlayerId}
          lastPlayerId={lastPlayerId}
        />
      </InfoWrapper>
    </FlexCol>
  );
};

export default OtherPlayer;
