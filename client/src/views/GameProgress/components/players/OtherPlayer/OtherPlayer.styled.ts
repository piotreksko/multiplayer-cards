import { FlexRow } from '@components/global/Flex';
import styled from 'styled-components';

interface ContainerProps {
  rotation: string;
  isLeftPlayer?: boolean;
  isRightPlayer?: boolean;
}

export const Container = styled.div<ContainerProps>`
  display: flex;
  transform-origin: top left;
  transform: ${(props) => `rotate(${props.rotation}deg)}`};
  ${(props) =>
    (props.isLeftPlayer || props.isRightPlayer) &&
    `
  transform: rotate(${props.rotation}deg) translate(-100%)}
  position: relative;
  white-space: nowrap;
  margin-top: -50%;
`}
`;

export const InfoWrapper = styled(FlexRow)`
  width: 100%;
`;
