import { FlexCol } from '@components/global/Flex';
import styled from 'styled-components';

export const Container = styled(FlexCol)`
  justify-content: space-around;

  @media screen and (max-width: 1200px) {
    max-width: 25vw;
  }

  @media screen and (max-width: 900px) {
    max-width: 10vw;
  }
`;
