import React from 'react';
import { Player } from 'types/Player';
import OtherPlayer from '../OtherPlayer/OtherPlayer';
import { Container } from './SidePlayers.styled';

interface Props {
  players: Player[];
  isGameOver: boolean;
  nextPlayerId: string;
  lastPlayerId: string;
  isLeftPlayers?: boolean;
}

const SidePlayers = ({
  players = [],
  isGameOver,
  nextPlayerId,
  lastPlayerId,
  isLeftPlayers
}: Props) => {
  if (!players.length) return null;
  return (
    <Container>
      {players.map((player) => (
        <OtherPlayer
          key={player._id}
          player={player}
          isGameOver={isGameOver}
          isLeftPlayer={isLeftPlayers}
          nextPlayerId={nextPlayerId}
          lastPlayerId={lastPlayerId}
        />
      ))}
    </Container>
  );
};

export default SidePlayers;
