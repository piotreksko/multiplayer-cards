import { cardRanks, cardRanksArray, cardSuits } from '@constants/cards';
import React, { useState } from 'react';
import { useMutation } from '@apollo/react-hooks';
import confirmMoveMutation from 'apollo/mutations/confirmMove';
import { useAppContext } from '@context/AppContext';
import { canFinishWithStairs, cardAmountsByRank } from '@scripts/gameLogic';
import PlayerCards from '../PlayerCards/PlayerCards';
import PlayerInfo from '../PlayerInfo/PlayerInfo';
import { Container, ButtonsContainer } from './PlayerDisplay.styled';
import { GameProgress } from 'types/Gameroom';
import { Player } from 'types/Player';
import { Card, CardRank, CardSuit } from 'types/Card';
import PassMoveButton from '@components/buttons/PassMoveButton/PassMoveButton';
import ConfirmMoveButton from '@components/buttons/ConfirmMoveButton/ConfirmMoveButton';

interface Props {
  gameProgressId: string;
  progress: GameProgress;
}

const PlayerDisplay = ({ gameProgressId, progress }: Props) => {
  const { user: { _id: userid = '' } = {} } = useAppContext();
  const [selectedCards, setSelectedCards] = useState<Card[]>([]);
  const [confirmMove] = useMutation(confirmMoveMutation, {
    variables: {
      gameProgressId,
      cardIds: selectedCards.map(({ _id }) => _id)
    }
  });
  const confirmAndClear = () => {
    confirmMove();
    setSelectedCards([]);
  };

  const player = progress.players.find(({ _id }) => _id === userid) as Player;
  const isPlayerMove = progress.nextPlayerId === userid;
  const hasPlayerWon = progress.winners.some(({ _id }) => _id === userid);
  const showButtons = !hasPlayerWon && !progress.isGameOver;
  const playerStartsRound =
    !progress.pile.length || progress.lastPlayerId === userid;

  const confirmButtonDisabled = () => {
    if (
      !progress.cardsSwapDone &&
      player.hasToSwap &&
      selectedCards.length === progress.cardsToSwapCount
    ) {
      return true;
    }

    const countCardsByRank = (rank: CardRank) =>
      selectedCards.filter((card) => card.rank === rank).length;

    const usedThreeSix = countCardsByRank(CardRank.SIX) === 3;
    const usedFourJacks = countCardsByRank(CardRank.JACK) === 4;
    const usedFiveAces = countCardsByRank(CardRank.ACE) === 5;

    if (usedThreeSix || usedFourJacks || usedFiveAces) {
      return true;
    }

    if (!selectedCards.length) return false;

    if (canFinishWithStairs(player.cards, progress, userid)) return true;

    const amounts = cardAmountsByRank(selectedCards);
    const differentRanks = [...new Set(selectedCards.map((card) => card.rank))];
    const hasSameAmountOfEachCard = amounts.length === 1;
    const hasOneOrThreeDifferentRanks =
      differentRanks.length === 1 || differentRanks.length === 3;

    if (playerStartsRound)
      return hasSameAmountOfEachCard && hasOneOrThreeDifferentRanks;

    const redTwosCount = selectedCards.filter(
      (card) => card.rank === CardRank.TWO && card.suit === CardSuit.HEARTS
    ).length;

    const lastTurn =
      progress.lastThreeTurns[progress.lastThreeTurns.length - 1];
    const isWeakestCardHigher =
      cardRanksArray.indexOf(selectedCards[0].rank) >
      cardRanksArray.indexOf(lastTurn.cards[lastTurn.cards.length - 1].rank);

    return (
      (hasSameAmountOfEachCard &&
        hasOneOrThreeDifferentRanks &&
        amounts[0] === progress.allowedCardsAmount &&
        isWeakestCardHigher) ||
      (redTwosCount > 0 &&
        amounts[0] + redTwosCount >= progress.allowedCardsAmount)
    );
  };

  return (
    <Container>
      <PlayerInfo
        player={player}
        nextPlayerId={progress.nextPlayerId}
        lastPlayerId={progress.lastPlayerId}
      />
      <PlayerCards
        progress={progress}
        selectedCards={selectedCards}
        setSelectedCards={setSelectedCards}
      />
      {showButtons && (
        <ButtonsContainer>
          <PassMoveButton
            gameProgressId={gameProgressId}
            disabled={!isPlayerMove || player.hasToSwap || playerStartsRound}
            setSelectedCards={setSelectedCards}
          />
          <ConfirmMoveButton
            confirmMove={confirmAndClear}
            disabled={!confirmButtonDisabled()}
          />
        </ButtonsContainer>
      )}
    </Container>
  );
};

export default PlayerDisplay;
