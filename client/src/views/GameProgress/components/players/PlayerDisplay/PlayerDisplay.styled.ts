import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 3rem;
`;

export const ButtonsContainer = styled.div`
  display: flex;
  justify-content: center;
`;
