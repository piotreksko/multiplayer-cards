import React from 'react';
import { Player } from 'types/Player';
import OtherPlayer from '../OtherPlayer/OtherPlayer';
import { Container } from './TopPlayers.styled';

interface Props {
  players: Player[]; 
  isGameOver: boolean;
  nextPlayerId: string;
  lastPlayerId: string;
}

const TopPlayers = ({
  players = [],
  isGameOver,
  nextPlayerId,
  lastPlayerId,
}: Props) => (
  <Container>
    {players.map((player) => (
      <OtherPlayer
        key={player._id}
        player={player}
        isGameOver={isGameOver}
        nextPlayerId={nextPlayerId}
        lastPlayerId={lastPlayerId}
      />
    ))}
  </Container>
);

export default TopPlayers;
