import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
`;

export const IconWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 36px;
  width: 36px;
  background-color: rgba(41, 48, 67, 0.4);
  border-radius: 8px;
  box-shadow: 0px 0px 10px 2px rgb(0 0 0 / 30%);
  margin: 0.5rem;
`;

export const Icon = styled.div<{ url: string }>`
  display: flex;
  align-items: center;
  justify-content: center;
  background-image: url(${(props) => props.url});
  height: 24px;
  width: 24px;
  background-size: contain;
  background-repeat: no-repeat;
  margin-top: 3px;
  margin-left: 4px;
`;

export const AllowedText = styled.div`
  margin-right: 8px;
  margin-bottom: 4px;
  color: white;
  font-family: 'Almendra', serif;
`;

export const StairsText = styled.div`
  margin-right: 18px;
  margin-bottom: 18px;
  color: white;
  font-family: 'Almendra', serif;
`;
