import React from 'react';
import {
  Container,
  IconWrapper,
  Icon,
  AllowedText,
  StairsText,
} from './ProgressIndicators.styled';
import stairs from 'content/images/stairs_white.png';
import emptyCards from 'content/images/empty_cards4.png';
import { GameProgress } from 'types/Gameroom';

const ProgressIndicators = ({ progress }: {progress: GameProgress}) => {
  return (
    <Container>
      {progress.allowedCardsAmount && (
        <IconWrapper>
          <Icon url={emptyCards}>
            <AllowedText>{progress.allowedCardsAmount}</AllowedText>
          </Icon>
        </IconWrapper>
      )}
      {progress.stairsActivated && (
        <IconWrapper>
          <Icon url={stairs}>
            <StairsText>{progress.stairsStep}</StairsText>
          </Icon>
        </IconWrapper>
      )}
    </Container>
  );
};

export default ProgressIndicators;
