import React from 'react';
import { useAppContext } from '@context/AppContext';
import SidePlayers from '../players/SidePlayers/SidePlayers';
import PlayerDisplay from '../players/PlayerDisplay/PlayerDisplay';
import TopPlayers from '../players/TopPlayers/TopPlayers';
import Pile from '../Pile/Pile';
import GameInfo from '../../../Gameroom/components/GameroomLobby/GameroomLobby';
import {
  MainContainer,
  MainRow,
  Column,
  Section
} from './GameProgressDisplay.styled';
import { GameProgress } from 'types/Gameroom';
import { Player } from 'types/Player';

interface Props {
  progress: GameProgress;
  gameProgressId: string;
  gameInProgress: boolean;
}

const GameProgressDisplay = ({
  progress,
  gameProgressId,
  gameInProgress
}: Props) => {
  const {
    user: { _id: userid }
  } = useAppContext();

  const getSidesCount = (players: Player[]) => {
    let topPlayersCount = 1;
    let rightPlayersCount = 0;
    let leftPlayersCount = 0;

    const addTop = () => {
      topPlayersCount += 1;
    };
    const addRight = () => {
      rightPlayersCount += 1;
    };
    const addLeft = () => {
      leftPlayersCount += 1;
    };

    const strategy = {
      3: () => addRight(),
      4: () => addLeft(),
      5: () => addTop(),
      6: () => addRight(),
      7: () => addLeft()
    };

    for (let i = 3; i < players.length + 1; i += 1) {
      strategy[i as 3 | 4 | 5 | 6 | 7]();
    }

    return { topPlayersCount, rightPlayersCount, leftPlayersCount };
  };

  const getSidePlayers = (startingIndex: number, allPlayers: Player[]) => {
    const { topPlayersCount, rightPlayersCount, leftPlayersCount } =
      getSidesCount(allPlayers);

    const distribution: {
      [key: string]: { count: number; players: Player[] };
    } = {
      leftSide: { count: leftPlayersCount, players: [] },
      topSide: { count: topPlayersCount, players: [] },
      rightSide: { count: rightPlayersCount, players: [] }
    };

    let lastPlayerIndex = startingIndex;

    Object.values(distribution).forEach(({ count, players }) => {
      for (let i = 0; i < count; i += 1) {
        const nextPlayerIndex = lastPlayerIndex + 1;
        const playerExists = !!allPlayers[nextPlayerIndex];
        if (playerExists) {
          players.push(allPlayers[nextPlayerIndex]);
          lastPlayerIndex = nextPlayerIndex;
        } else {
          players.push(allPlayers[0]);
          lastPlayerIndex = 0;
        }
      }
    });

    return distribution;
  };

  const distributePlayers = (players: Player[]) => {
    const playerIndex = players.findIndex(({ _id }) => {
      return _id === userid;
    });

    const isObserver = playerIndex === -1;
    const startingIndex = !isObserver ? playerIndex : 0;

    return getSidePlayers(startingIndex, players);
  };
  const {
    leftSide: { players: leftPlayers },
    rightSide: { players: rightPlayers },
    topSide: { players: topPlayers }
  } = distributePlayers(progress.players);

  return (
    <MainContainer>
      <GameInfo progress={progress} gameInProgress={gameInProgress} />
      <MainRow>
        <Column>
          <SidePlayers
            players={leftPlayers}
            isGameOver={progress.isGameOver}
            nextPlayerId={progress.nextPlayerId}
            lastPlayerId={progress.lastPlayerId}
            isLeftPlayers
          />
        </Column>
        <Column mainColumn>
          <Section>
            <TopPlayers
              players={topPlayers}
              isGameOver={progress.isGameOver}
              nextPlayerId={progress.nextPlayerId}
              lastPlayerId={progress.lastPlayerId}
            />
          </Section>
          <Section height={'25%'}>
            <Pile
              cards={progress.pile}
              lastThreeTurns={progress.lastThreeTurns}
              progress={progress}
            />
          </Section>
          <Section>
            <PlayerDisplay
              gameProgressId={gameProgressId}
              progress={progress}
            />
          </Section>
        </Column>
        <Column>
          <SidePlayers
            players={rightPlayers}
            isGameOver={progress.isGameOver}
            nextPlayerId={progress.nextPlayerId}
            lastPlayerId={progress.lastPlayerId}
          />
        </Column>
      </MainRow>
    </MainContainer>
  );
};

export default GameProgressDisplay;
