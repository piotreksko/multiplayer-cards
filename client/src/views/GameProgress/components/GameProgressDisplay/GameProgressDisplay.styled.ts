import { FlexCol, FlexRow } from '@components/global/Flex';
import styled from 'styled-components';

export const MainContainer = styled(FlexCol)`
  align-items: center;

  /* @media screen and (max-height: 930px) {
    height: 100vh;
  } */
`;

export const MainRow = styled(FlexRow)`
  transform-origin: center top;

  @media (max-aspect-ratio: 420/700) {
    transform-origin: center;
  }

  @media screen and (max-width: 820px), screen and (max-height: 990px) {
    transform: scale(0.95);
  }

  @media screen and (max-width: 820px), screen and (max-height: 900px) {
    transform: scale(0.9);
  }

  @media screen and (max-width: 720px), screen and (max-height: 850px) {
    transform: scale(0.85);
  }

  @media screen and (max-width: 680px), screen and (max-height: 780px) {
    transform: scale(0.8);
  }

  @media screen and (max-width: 625px), screen and (max-height: 740px) {
    transform: scale(0.75);
  }

  @media screen and (max-width: 580px), screen and (max-height: 700px) {
    transform: scale(0.7);
  }

  @media screen and (max-width: 540px), screen and (max-height: 660px) {
    transform: scale(0.65);
  }
`;

export const Column = styled(FlexCol)<{ mainColumn?: boolean }>`
  min-width: 170px;
  width: ${(props) => (props.mainColumn ? '80%' : '20%')};
  ${(props) => props.mainColumn && 'max-width: 1000px;'}
`;

export const Section = styled.div<{ height?: string }>`
  height: ${(props) => props.height || '30%'};
`;
