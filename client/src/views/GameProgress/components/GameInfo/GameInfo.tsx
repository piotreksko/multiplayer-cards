import React from 'react';
import { Player } from 'types/Player';
import './GameInfo.scss';

const styles = {
  green: '#21eb6e',
  yellow: 'yellow',
};

interface Props {
  gameroomName: string;
  hostName: string;
  players: Player[]
}

function GameInfo({ gameroomName, hostName, players }: Props) {
  return (
    <div className="game-info">
      <div className="gameroom-title">
        <h1>{gameroomName}</h1>
        <h2>Host: {hostName}</h2>
        <figure className="avatar">
          <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/156381/profile/profile-80.jpg" />
        </figure>
      </div>
      <div className="users">
        <div className="users-content">
          {players.map((player) => (
            <div key={player.username}>
              <span
                style={{ color: player.isReady ? styles.green : styles.yellow }}
              >
                {player.username}
              </span>
              <br />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default GameInfo;
