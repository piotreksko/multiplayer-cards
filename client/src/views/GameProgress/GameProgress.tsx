import React from 'react';
import { useQuery, useSubscription } from '@apollo/react-hooks';
import updateGameProgress from '../../apollo/subscriptions/updateGameProgress';
import getGameProgressQuery from 'apollo/queries/getGameProgress';
import { GameProgress as GameProgressInt } from 'types/Gameroom';
import GameProgressDisplay from './components/GameProgressDisplay/GameProgressDisplay';

interface GameProgressProps {
  gameProgressId: string;
  gameInProgress: boolean;
}

const GameProgress: React.FC<GameProgressProps> = ({
  gameProgressId,
  gameInProgress
}) => {
  const { data, loading, error } = useQuery<
    { getGameProgress: GameProgressInt },
    { gameProgressId: string }
  >(getGameProgressQuery, {
    variables: {
      gameProgressId
    }
  });

  useSubscription(updateGameProgress, {
    variables: { gameProgressId },
    onSubscriptionData: ({ client, subscriptionData }) => {
      if (!subscriptionData.data) {
        return;
      }
      client.writeQuery({
        query: getGameProgressQuery,
        data: {
          getGameProgress: {
            ...data?.getGameProgress,
            ...subscriptionData.data.updateGameProgress
          }
        }
      });
    }
  });

  if (loading) return <div>Loading</div>;
  if (error) return <div>Error</div>;

  const progress = data?.getGameProgress || ({} as GameProgressInt);

  return (
    <>
      <GameProgressDisplay
        progress={progress}
        gameProgressId={gameProgressId}
        gameInProgress={gameInProgress}
      />
    </>
  );
};

export default GameProgress;
