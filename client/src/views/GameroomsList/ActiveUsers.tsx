import { ApolloError, useQuery } from '@apollo/react-hooks';
import React from 'react';
import getActiveUsersQuery from 'apollo/queries/getActiveUsers';
import subscribeToMoreUsers from 'apollo/subscriptions/updateActiveUsers';
import { User } from 'types/User';

function ActiveUsersWrapper() {
  const { data, loading, error, subscribeToMore } =
    useQuery(getActiveUsersQuery);

  return (
    <ActiveUsers
      data={data}
      loading={loading}
      error={error}
      subscribeToMoreUsers={() => {
        subscribeToMore({
          document: subscribeToMoreUsers,
          updateQuery: (previousResult, { subscriptionData }) => {
            if (!subscriptionData.data) {
              return previousResult;
            }

            const updatedActiveUsers = subscriptionData.data.updateActiveUsers;

            return {
              ...previousResult,
              updateActiveUsers: [
                ...previousResult.updateActiveUsers,
                ...updatedActiveUsers
              ]
            };
          }
        });
      }}
    />
  );
}

const ActiveUsers = ({
  data,
  loading,
  error
}: {
  data: {
    getActiveUsers: User[];
  };
  loading: boolean;
  error: ApolloError | undefined;
  subscribeToMoreUsers: () => void;
}) => {
  return (
    <div>
      <ul>
        {!loading &&
          data.getActiveUsers.map((user: User) => (
            <li key={user._id}>{user.username}</li>
          ))}
      </ul>
    </div>
  );
};

export default ActiveUsersWrapper;
