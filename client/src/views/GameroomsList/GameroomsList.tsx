import React from 'react';
import { useQuery, useMutation, ApolloError } from '@apollo/react-hooks';
import './GameroomsList.scss';
import getGameroomsQuery from 'apollo/queries/getGamerooms';
import createGameroomMutation from 'apollo/mutations/createGameroom';
import { useHistory, withRouter } from 'react-router';
import JoinButton from '../../components/buttons/JoinGameButton/JoinGameButton';
import withAuth from '../../withAuth';
import ActiveUsers from './ActiveUsers';
import colors from '@constants/colors';
import { getGameroomRoute } from '@constants/routes';
import { Gameroom } from 'types/Gameroom';

import { FlexRow } from '../../components/global/Flex';
import styled from 'styled-components';
import { Player } from 'types/Player';

function GameroomsList() {
  const history = useHistory();

  const { data, loading, error } = useQuery(getGameroomsQuery);

  const renderUsers = (players: Player[]) => {
    if (!players) return 'No active users';
    const usernames = players.map((player) => player.username);
    return `${usernames.length} players: ${usernames.join(', ')}`;
  };

  const renderGamerooms = ({
    getGamerooms: gamerooms
  }: {
    getGamerooms: Gameroom[];
  }) => {
    const renderedGamerooms = gamerooms.map((gameroom) => (
      <div className="list-item" key={gameroom._id}>
        {gameroom.name} - {renderUsers(gameroom.players)}
        <JoinButton gameroomId={gameroom._id} />
      </div>
    ));

    return renderedGamerooms;
  };

  const [createGameroom] = useMutation(createGameroomMutation);

  const handleCreateGameroom = async (name: string) => {
    const {
      data: createData,
      loading: createLoading,
      error: createError
    } = (await createGameroom({
      variables: { name }
    })) as {
      data: { createGameroom: Gameroom };
      loading: boolean;
      error?: ApolloError;
    };

    if (!createLoading && !createError) {
      history.push(getGameroomRoute(createData.createGameroom._id));
    }
  };

  const handleCreateClick = () => {
    // eslint-disable-next-line no-alert
    const name = prompt('Gameroom name:', '');
    if (name) {
      handleCreateGameroom(name);
    }
  };

  return (
    <>
      <div className="gamerooms">
        <Header>
          <h1>Gamerooms</h1>
          <Button onClick={handleCreateClick} color={colors.positive}>
            Create gameroom
          </Button>
        </Header>
        {error && 'error'}
        {!loading && !error && renderGamerooms(data)}
      </div>
      <ActiveUsers />
    </>
  );
}

const Header = styled(FlexRow)`
  justify-content: space-between;
`;

const Button = styled.button`
  width: 8rem;
  height: 90%;
  float: right;
`;

export default withRouter(withAuth(GameroomsList));
