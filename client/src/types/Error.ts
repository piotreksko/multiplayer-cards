export interface GraphqlError {
  graphQLErrors: {
    message: string;
  }[];
  message: string;
}

export enum ErrorCode {
  USERNAME_TAKEN = 'Username is already taken. Please choose another one'
}
