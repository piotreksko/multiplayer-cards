import { Card } from './Card';

export interface Player {
  _id: string;
  username: string;
  cardsCount: number;
  cards: Card[];
  hasToSwap: boolean;
  swapsBestCards: boolean;
  cardsToSwapCount: number;
  isReady: boolean;
}
