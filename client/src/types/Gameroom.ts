import { Card } from './Card';
import { Player } from './Player';
import { User } from './User';

export interface Gameroom {
  _id: string;
  name: string;
  host: User;
  players: Player[];
  gameInProgress: boolean;
  gameProgressId: string;
  gameProgress: GameProgress;
}

export interface GameProgress {
  _id: string;
  players: Player[];
  pile: Card[];
  lastThreeTurns: { cards: Card[]; playerId: string }[];
  lastPlayerId: string;
  nextPlayerId: string;
  stairsActivated: boolean;
  stairsStep: number;
  allowedCardsAmount: number;
  isNewRound: boolean;
  isGameOver: boolean;
  winners: Player[];
  lastGameWinners: Player[];
  grandmaster: boolean;
  cardsSwapDone: boolean;
  cardsToSwapCount: number;
}
