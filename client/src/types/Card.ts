export enum CardRank {
  TWO = '2',
  ACE = 'ACE',
  KING = 'KING',
  QUEEN = 'QUEEN',
  JACK = 'JACK',
  TEN = '10',
  NINE = '9',
  EIGHT = '8',
  SEVEN = '7',
  SIX = '6',
  FIVE = '5',
  FOUR = '4',
  THREE = '3'
}

export enum CardSuit {
  HEARTS = 'HEART',
  DIAMONDS = 'DIAMONDS',
  SPADES = 'SPADES',
  CLUBS = 'CLUBS'
}

export interface Card {
  _id: number;
  suit: CardSuit;
  rank: CardRank;
  available: boolean;
  selected: boolean;
  possible: boolean;
}
