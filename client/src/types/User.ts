export interface UserLogIn {
    _id: string;
    username: string
}

export interface User {
    _id: string;
    username: string;
    isReady: boolean;
}