export interface Message {
	_id: string;
	text: string;
	createdBy: string;
	createdAt: string;
}