module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  testEnvironmentOptions: {},
  moduleNameMapper: {
    'src(.*)$': '<rootDir>/src$1',
    // '^@/(.*)$': '<rootDir>/src/$1'
    // '^@/(.*)$': '<rootDir>/src/$1',
    '^@constants/(.*)$': '<rootDir>/src/constants/$1',
    '^@content/(.*)$': '<rootDir>/src/content/$1',
    '^@context/(.*)$': '<rootDir>/src/context/$1',
    '^@components/(.*)$': '<rootDir>/src/components/$1'
  },
  moduleDirectories: ['node_modules', 'src'],
  collectCoverageFrom: [
    'src/**/*.{js,jsx,ts,tsx}',
    '!src/**/*.d.ts',
    '!src/**/*.{test,spec}.{js,jsx,ts,tsx}',
    '!src/index.tsx'
  ],
  coverageThreshold: {
    global: {
      statements: 80,
      branches: 80,
      functions: 80,
      lines: 80
    }
  },
  transform: {
    '.+\\.(css|scss|png|jpg|svg)$': 'jest-transform-stub',
    '^.+\\.(js|jsx|ts|tsx)$': [
      'ts-jest',
      { tsconfig: 'tsconfig.json', diagnostics: false }
    ]
  },
  setupFilesAfterEnv: ['@testing-library/jest-dom/extend-expect'],
  testPathIgnorePatterns: ['/build/', '/node_modules/', '/public/', '/scripts/']
};
